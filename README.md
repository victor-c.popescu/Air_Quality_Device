# Air Quality Device



## Sistem Pentru Detecția Calității Aerului din Interiorul Locuințelor

Popescu Victor-Cristian, student la facultatea de Automatică și Calculatoare, secția CTI-RO 

Adresă repository: https://gitlab.upt.ro/victor-c.popescu/Air_Quality_Device.git


## Fisiere

Pachetul vine cu 3 directoare diferite cu numele MEGA, UNO, NANO. odată descărcate pe un laptop/calclator, acestea trebuie să se afle în 3 foldere diferite a căror nume se fie identic cu cel al fisierelor .ino

## Instalare

Pentru instalarea aplicației este necesar să aveți instalat pe laptop/calculator un mediu de dezvoltare compatibil cu Arduino și 3 plăci de dezvoltare originale Arduino sau compatibile cu acestea. Cele trei modele sunt: Ardunio Mega, Arduino UNO R4 WiFi, Arduino Nano

## Instalare Mega

În mediul de dezvoltare selectați optiunea Tools -> Board -> Arduino AVR Boards și selectați opțiunea Arduino Mega or Mega 2560

Pentru a realiza comunicația cu placa introduceți un cablu în portul usb al plăcii și ulterior în portul usb al laptopului/calculatorului. Selectați Tools -> Port și selectați portul care este disponibil. Dacă nu apare niciun port înseamnă că placa nu este văzută de laptop. Verificați cu un alt cablu sau port.

Pentru a putea rula codul aveți nevoie să instalați librariile folosite. Acestea sunt:
> MCUFRIEND_kbv.h
> Adafruit_GFX.h
> Adafruit_TFTLCD.h
> TouchScreen.h
> UTFTGLUE.h
> virtuabotixRTC.h
> RTClib.h
> SD.h

Pentru a instala librariile selectați Tools -> Manage Libraries și căutați după nume librariile.

Dacă libraria nu a fost găsită, va trebui descărcată online ca și arhivă zip și instalaltă selectând Sketch -> Inculde Library -> Add .ZIP Library.

Odată instalate toate librăriile și conectarea la placă, selectați butonul de upload.

## Instalare UNO R4 WiFi

În mediul de dezvoltare selectați optiunea Tools -> Board -> Board Manager, căutați Arduino UNO R4 Boards și instalați pachetul.
Ulterior selectați Tools -> Board -> Arduino UNO R4 Boards -> Arduino UNO R4 Wifi.

Pentru a realiza comunicația cu placa introduceți un cablu în portul usb al plăcii și ulterior în portul usb al laptopului/calculatorului. Selectați Tools -> Port și selectați portul care este disponibil. Dacă nu apare niciun port înseamnă că placa nu este văzută de laptop. Verificați cu un alt cablu sau port.

Pentru a putea rula codul aveți nevoie să instalați librariile folosite. Acestea sunt:
> Wire.h
> Adafruit_Sensor.h
> Adafruit_LIS3DH.h
> Adafruit_SGP30.h
> Adafruit_BME680.h
> DFRobot_MICS.h
> Servo.h
> WiFiS3.h

Pentru a instala librariile selectați Tools -> Manage Libraries și căutați după nume librariile.

Dacă libraria nu a fost găsită, va trebui descărcată online ca și arhivă zip și instalaltă selectând Sketch -> Inculde Library -> Add .ZIP Library.

Odată instalate toate librăriile și conectarea la placă, selectați butonul de upload.

## Instalare Nano

În mediul de dezvoltare selectați optiunea Tools -> Board -> Arduino AVR Boards și selectați opțiunea Arduino Nano.

Pentru a realiza comunicația cu placa introduceți un cablu în portul usb al plăcii și ulterior în portul usb al laptopului/calculatorului. Selectați Tools -> Port și selectați portul care este disponibil. Dacă nu apare niciun port înseamnă că placa nu este văzută de laptop. Verificați cu un alt cablu sau port.

Pentru a putea rula codul aveți nevoie să instalați librariile folosite. Acestea sunt:
> KarserDSM501.h
> Wire.h

Pentru a instala librariile selectați Tools -> Manage Libraries și căutați după nume librariile.

Dacă libraria nu a fost găsită, va trebui descărcată online ca și arhivă zip și instalaltă selectând Sketch -> Inculde Library -> Add .ZIP Library.

Odată instalate toate librăriile și conectarea la placă, selectați butonul de upload.



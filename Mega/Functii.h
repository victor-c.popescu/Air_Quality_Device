#ifndef FUNCTII_H
#define FUNCTII_H

#include <String.h>
#include <stdint.h>
#include <SD.h>
#include <SPI.h>
#include <TouchScreen.h>
#include <UTFTGLUE.h>
#include <virtuabotixRTC.h>
#include <RTClib.h>

extern File myFile;  //creare fisier myFile
extern virtuabotixRTC myRTC;  //creare obiect myRTC pentru modulul de timp - pini = data/clock/reset
extern bool UpdateStatus;
extern bool pageChanged;
extern bool tempFlag;
extern bool presFlag;
extern bool humFlag;
extern bool eco2Flag;
extern bool dustFlag;
extern bool tvocFlag;
extern bool pm10Flag;
extern bool pm25Flag;
extern bool wifiFlag;
extern bool errorFlag;
extern bool ip_print_flag;
extern bool sent_wifi_request_flag;
extern int temperatura, umiditate, presiune, eco2, tvoc;
extern int sensor_status;
extern String dust;
extern String combined;
extern String ip_adress;
extern float pm10,pm25;


extern String Fisier_Temperatura;
extern String Fisier_Umiditate;
extern String Fisier_Eco2;
extern String Fisier_TVOC;
extern String Fisier_Time;

extern int pixel_x, pixel_y; 


uint64_t cutStringIntoWords(String inputString);

int extractValueAfterArrow(String inputString);

void write_file(int value, String Fisier);

uint64_t cuttime();

void UpdateTime_FromFile();

void readvalues();

void readstatus();

void readWIFI();

String readSSIDAndPassword();

int get_last_7_days(int previousDay[7]);

void read_lines(String FileToRead, int cont[]);

bool Touch_getXY(int *pixel_x, int *pixel_y);

#endif
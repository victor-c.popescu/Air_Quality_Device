#include "Page_Management.h"
#include "Functii.h"

MCUFRIEND_kbv tft;  //creare obiect tft pentru controlul lcd-ului

int last7[7];
int contor[11]; //vector pentru stocarea sumei valorilor citite/ in final stocarea mediei lor
int col[8];

int previousMinutes = -1;
int previousHours = -1;
int previousDay = -1;
int previousMonth = -1;
int previousYear = -1;


// Definire butoane
Adafruit_GFX_Button TVOC_btn;
Adafruit_GFX_Button TPUCO_btn;
Adafruit_GFX_Button stop_btn;
Adafruit_GFX_Button pag1_btn;
Adafruit_GFX_Button istoric_btn;
Adafruit_GFX_Button about_btn;
Adafruit_GFX_Button Menu_btn;
Adafruit_GFX_Button Temperature_Graph_btn;
Adafruit_GFX_Button Humidity_Graph_btn;
Adafruit_GFX_Button Eco2_Graph_btn;
Adafruit_GFX_Button TVOC_Graph_btn;
Adafruit_GFX_Button Back_to_GraphPage_btn;
Adafruit_GFX_Button UpdateTime_btn;
Adafruit_GFX_Button Check_btn;
Adafruit_GFX_Button Check_SD_btn;
Adafruit_GFX_Button Back_to_Check_btn;
Adafruit_GFX_Button Check_Hardware_btn;
Adafruit_GFX_Button Buzzer_Start_btn;
Adafruit_GFX_Button Buzzer_Stop_btn;
Adafruit_GFX_Button Power_Trigger_Start_btn;
Adafruit_GFX_Button Power_Trigger_Stop_btn;
Adafruit_GFX_Button Gas_Trigger_Start_btn;
Adafruit_GFX_Button Gas_Trigger_Stop_btn;
Adafruit_GFX_Button Air_Vent_Start_btn;
Adafruit_GFX_Button Air_Vent_Stop_btn;
Adafruit_GFX_Button WIFI_btn;
Adafruit_GFX_Button Time_btn;
Adafruit_GFX_Button Back_btn;
Adafruit_GFX_Button Enable_btn;
Adafruit_GFX_Button Disable_btn;

Page currentPage = PAGEINITIALIZARE; // Start with Page 1


 void Buttons_Initialization(){
Menu_btn.initButton(&tft, 70, 300, 130, 40, WHITE, CYAN, BLACK, "Meniu", 2);
Back_to_GraphPage_btn.initButton(&tft, 70, 30, 100, 40, WHITE, CYAN, BLACK, "Back", 2);
TVOC_btn.initButton(&tft, 410, 300, 130, 40, WHITE, CYAN, BLACK, "T/DST", 2);
TPUCO_btn.initButton(&tft, 240, 300, 130, 40, WHITE, CYAN, BLACK, "TUPCO", 2);
stop_btn.initButton(&tft, 380, 280, 150, 40, WHITE, CYAN, BLACK, "Stop", 2);
pag1_btn.initButton(&tft, 250, 100, 285, 60, WHITE, WHITE, GREEN, "Air Q", 3);
istoric_btn.initButton(&tft, 250, 180, 285, 60, WHITE, WHITE, GREEN, "Grafice", 3);
about_btn.initButton(&tft, 250, 260, 285, 60, WHITE, WHITE, GREEN, "Setari", 3);
Temperature_Graph_btn.initButton(&tft, 130, 120, 200, 60, WHITE, WHITE, GREEN, "Temp", 3);
Humidity_Graph_btn.initButton(&tft, 360, 120, 200, 60, WHITE, WHITE, GREEN, "Humidity", 3);
Eco2_Graph_btn.initButton(&tft, 130, 210, 200, 60, WHITE, WHITE, GREEN, "Eco2", 3);
TVOC_Graph_btn.initButton(&tft, 360, 210, 200, 60, WHITE, WHITE, GREEN, "TVOC", 3);
UpdateTime_btn.initButton(&tft, 250, 100, 285, 50, WHITE, WHITE, GREEN, "Actualizare", 2);
Check_btn.initButton(&tft, 250, 170, 285, 50, WHITE, WHITE, GREEN, "Check", 2);
Check_Hardware_btn.initButton(&tft, 250, 240, 285, 50, WHITE, WHITE, GREEN, "Hardware", 2);
Check_SD_btn.initButton(&tft, 410, 300, 130, 40, WHITE, WHITE, BLACK, "SD", 2);
Back_to_Check_btn.initButton(&tft, 240, 300, 130, 40, WHITE, WHITE, BLACK, "Sensors", 2);
Buzzer_Start_btn.initButton(&tft, 250, 85, 130, 40, WHITE, WHITE, BLACK, "Start", 2);
Buzzer_Stop_btn.initButton(&tft, 410, 85, 130, 40, WHITE, WHITE, BLACK, "Stop", 2);
Power_Trigger_Start_btn.initButton(&tft, 250, 135, 130, 40, WHITE, WHITE, BLACK, "Power Off", 2);
Power_Trigger_Stop_btn.initButton(&tft, 410, 135, 130, 40, WHITE, WHITE, BLACK, "Power On", 2);
Gas_Trigger_Start_btn.initButton(&tft, 250, 190, 130, 40, WHITE, WHITE, BLACK, "Gas Off", 2);
Gas_Trigger_Stop_btn.initButton(&tft, 410, 190, 130, 40, WHITE, WHITE, BLACK, "Gas On", 2);
Air_Vent_Start_btn.initButton(&tft, 250, 245, 130, 40, WHITE, WHITE, BLACK, "Open", 2);
Air_Vent_Stop_btn.initButton(&tft, 410, 245, 130, 40, WHITE, WHITE, BLACK, "Close", 2);
WIFI_btn.initButton(&tft, 250, 210, 285, 70, WHITE, WHITE, GREEN, "WIFI", 2);
Time_btn.initButton(&tft, 250, 110, 285, 70, WHITE, WHITE, GREEN, "Time", 2);
Back_btn.initButton(&tft, 70, 300, 130, 40, WHITE, CYAN, BLACK, "Back", 2);
Enable_btn.initButton(&tft, 160, 240, 140, 50, WHITE, WHITE, GREEN, "Enable", 2);
Disable_btn.initButton(&tft, 330, 240, 140, 50, WHITE, WHITE, GREEN, "Disable", 2);
 }


void drawPageSettings()
{
  Menu_btn.drawButton(true);
  UpdateTime_btn.drawButton(true);
  Check_btn.drawButton(true);
  Check_Hardware_btn.drawButton(true);
  tft.setTextColor(WHITE);
  tft.setTextSize(3);
  tft.setCursor(170, 20);
  tft.println("Settings");
  tft.drawLine(0, 55, 480, 55, WHITE);
}

void drawCheckHardware()
{  
  Menu_btn.drawButton(true);
  Buzzer_Start_btn.drawButton(true);
  Buzzer_Stop_btn.drawButton(true);
  Power_Trigger_Start_btn.drawButton(true);
  Power_Trigger_Stop_btn.drawButton(true);
  Gas_Trigger_Start_btn.drawButton(true);
  Gas_Trigger_Stop_btn.drawButton(true);
  Air_Vent_Start_btn.drawButton(true);
  Air_Vent_Stop_btn.drawButton(true);
  tft.setTextColor(WHITE);
    tft.setTextSize(3);
    tft.setCursor(100, 20);
    tft.println("Hardware Check");
  tft.drawLine(0, 55, 480, 55, WHITE);
  
    tft.setCursor(10, 80);
    tft.println("Buzzer");
  
  tft.setCursor(10, 130);
    tft.println("Power");
  
  tft.setCursor(10, 180);
    tft.println("Gas");
  
  tft.setCursor(10, 230);
    tft.println("Air Vent");
}

void drawCheckSystem()
{
  tft.fillScreen(BLACK);
  Menu_btn.drawButton(true);
  Check_SD_btn.drawButton(true);
  Back_to_Check_btn.drawButton(false);
   
  tft.setTextColor(WHITE);
  tft.setTextSize(3);
  tft.setCursor(50, 20);
  tft.println("Senzor");
  tft.setCursor(300, 20);
  tft.println("Status");
  tft.drawLine(0, 55, 480, 55, WHITE);
  
  tft.setCursor(90, 140);
  tft.println("CHECKING SENSORS...");
  tft.setCursor(125, 180);
  tft.println("Please wait!");
}


void drawCheckSD()
{
  tft.fillScreen(BLACK);
  Menu_btn.drawButton(true);

  tft.setTextColor(WHITE);
    tft.setTextSize(3);
    tft.setCursor(35, 20);
    tft.println("SD Initialization Check");
  tft.drawLine(0, 55, 480, 55, WHITE);
checkSD();
}

void drawPage1()
{
  tft.fillRect(25, 50, 200, 100, TFT_CYAN);
  tft.setTextSize(2.5);
  tft.setTextColor(BLACK);
  tft.setCursor(60, 70);
  tft.print("Temperatura");
  tft.setTextSize(3);
  tft.setCursor(140, 105);
  tft.print("*C");
  
  tft.fillRect(255, 50, 200, 100, TFT_CYAN);
  tft.setCursor(280, 70);
  tft.print("Umiditate");
  tft.setCursor(380, 110);
  tft.print("%");
  
  tft.fillRect(25, 170, 200, 100, TFT_CYAN);
  tft.setCursor(60, 190);
  tft.print("Presiune");
  tft.setCursor(150, 225);
  tft.print("hpa");

  tft.fillRect(255, 170, 200, 100, TFT_CYAN);
  tft.setCursor(320, 190);
  tft.print("eCO2");
  tft.setCursor(380, 225);
  tft.print("ppm");
  
  TVOC_btn.drawButton(true);
  TPUCO_btn.drawButton(false);
  Menu_btn.drawButton(true);
  
  tempFlag = true;
  presFlag = true;
  humFlag = true;
  eco2Flag = true;
}

void drawPage2()
{
  tft.setTextColor(BLACK);
  tft.setTextSize(3.5);
  tft.setCursor(180, 20);
  
  tft.fillRect(40, 40, 190, 230, TFT_CYAN); //dreptunghi pentru VOC
  tft.fillRect(250, 40, 200, 230, TFT_CYAN); //dreptunghi pentru TVOC

  tft.fillRect(40, 95, 95, 5, BLACK); //contur orizontal VOC
  tft.fillRect(130, 40, 5, 55, BLACK); //contur vertical VOC

  tft.fillRect(250, 145, 200, 20, BLACK); //contur orizontal TVOC
  
  tft.setCursor(50, 60);
  tft.print("TVOC");
  
  tft.setCursor(310, 60);
  tft.print("PM10");
  tft.setCursor(350, 100);
  tft.print("ug/m3");

  tft.setCursor(310, 180);
  tft.print("PM25");
    tft.setCursor(350, 220);
  tft.print("ug/m3");

  tft.fillRect(145, 45, 40, 210, BLACK); //contur bara afisare nivel VOC 
  tft.fillRect(150, 50, 30, 200, WHITE); //bara afisare nivel VOC

  TVOC_btn.drawButton(false);
  TPUCO_btn.drawButton(true);
  Menu_btn.drawButton(true);

  bool dustFlag = true;
  bool tvocFlag = true;
}

void drawPageGraphics()
{
tft.setTextColor(WHITE);
  tft.setTextSize(3);
  tft.setCursor(180, 20);
  tft.println("GRAPHICS");
  tft.drawLine(0, 55, 480, 55, WHITE);
  Temperature_Graph_btn.drawButton(true);
  Humidity_Graph_btn.drawButton(true);
  Eco2_Graph_btn.drawButton(true);
  TVOC_Graph_btn.drawButton(true);
  Menu_btn.drawButton(true);
}

void drawHome()
{  
  tft.fillScreen(BLACK);
  tft.setTextColor(WHITE);
  tft.setTextSize(3);
  tft.setCursor(200, 20);
  tft.println("Meniu");
  tft.drawLine(0, 55, 480, 55, WHITE);
  
  pag1_btn.drawButton(true);
  istoric_btn.drawButton(true);
  about_btn.drawButton(true);
}

void drawErrorSD()
{
tft.setTextColor(WHITE);
  tft.setTextSize(3);
  tft.setCursor(80, 100);
  tft.println("No SD Card Detected!");
  tft.setTextSize(2);
  tft.setCursor(80, 160);
  tft.println("The device will start without");
  tft.setCursor(60, 190);
  tft.println("graphics functionality available");
  delay(4000);
  

}

void drawPageInitializare()
{
  tft.setTextColor(GREEN);
  tft.setTextSize(4);
  tft.setCursor(180, 150);
  tft.println("Welcome");

}

void drawPageLeaking()
{
  tft.setTextColor(WHITE);
  tft.setTextSize(3);
  tft.setCursor(150, 150);
  tft.println("GAS LEAKING!!!!");
  stop_btn.drawButton(true);
}


void drawPageLoading()
{
  tft.setCursor(180, 160);
  tft.setTextColor(WHITE);
  tft.setTextSize(3);
  tft.println("Loading...");
delay(3000);

  tft.fillScreen(BLACK);
  

}

void checkSD()
{  
  Back_to_Check_btn.drawButton(true);
  Check_SD_btn.drawButton(false);
  tft.setTextSize(2);
   
  if (!SD.begin()) {
  tft.fillRect(0, 60, 480, 35, RED);
    tft.setCursor(50, 70);
    tft.println("SD CARD");
    tft.setCursor(320, 70);
    tft.println("Failed");
  }else{
  tft.fillRect(0, 60, 480, 35, GREEN);
    tft.setCursor(50, 70);
    tft.println("SD CARD");
    tft.setCursor(320, 70);
    tft.println("Successful");    
  }
  
  File myFile = SD.open("Temp.txt", FILE_READ);
  if (myFile) {
  tft.fillRect(0, 95, 480, 35, GREEN);
  tft.setCursor(50, 105);
    tft.println("Fisier Temperatura");
    tft.setCursor(320, 105);
    tft.println("Successful");
  }else{
  tft.fillRect(0, 95, 480, 35, RED);
  tft.setCursor(50, 105);
    tft.println("Fisier Temperatura");
    tft.setCursor(320, 105);
    tft.println("Failed");
  }
  
  myFile = SD.open("Umid.txt", FILE_READ);
  if(myFile){
  tft.fillRect(0, 130, 480, 35, GREEN);
  tft.setCursor(50, 140);
    tft.println("Fisier Umiditate");
    tft.setCursor(320, 140);
    tft.println("Successful");  
  }else{
  tft.fillRect(0, 130, 480, 35, RED);
  tft.setCursor(50, 140);
    tft.println("Fisier Umiditate");
    tft.setCursor(320, 140);
    tft.println("Failed"); 
  }
  
  myFile = SD.open("Eco2.txt", FILE_READ);
  if(myFile){
  tft.fillRect(0, 165, 480, 35, GREEN);
  tft.setCursor(50, 175);
    tft.println("Fisier Eco2");
    tft.setCursor(320, 175);
    tft.println("Successful");
  }else{
  tft.fillRect(0, 165, 480, 35, RED);
  tft.setCursor(50, 175);
    tft.println("Fisier Eco2");
    tft.setCursor(320, 175);
    tft.println("Failed");
  }
  
   myFile = SD.open("TVOC.txt", FILE_READ);
  if(myFile){
  tft.fillRect(0, 200, 480, 35, GREEN);
  tft.setCursor(50, 210);
    tft.println("Fisier TVOC");
    tft.setCursor(320, 210);
    tft.println("Successful");
  }else{
  tft.fillRect(0, 200, 480, 35, RED);
  tft.setCursor(50, 210);
    tft.println("Fisier TVOC");
    tft.setCursor(320, 210);
    tft.println("Failed");
  }
  
   myFile = SD.open("Time.txt", FILE_READ);
  if(myFile){
  tft.fillRect(0, 235, 480, 35, GREEN);
  tft.setCursor(50, 245);
    tft.println("Fisier Actualizare Ora");
    tft.setCursor(320, 245);
    tft.println("Successful");
  }else{
  tft.fillRect(0, 235, 480, 35, RED);
  tft.setCursor(50, 245);
    tft.println("Fisier Actualizare Ora");
    tft.setCursor(320, 245);
    tft.println("Failed");
  }
}

void drawGrafic(int x, int y, int z)
{
    get_last_7_days(last7);
  Back_to_GraphPage_btn.drawButton(true);
  tft.setTextColor(WHITE);
    tft.setTextSize(2);
  
  if(contor[1] > z){
    tft.fillRect(60, 65, 30, 200, RED);
  }else{
      tft.fillRect(60, 65, 30, 200, WHITE);
  }
  tft.fillRect(60, 65, 30, 200-map(contor[1], x, y, 0, 200), BLACK);
  tft.setCursor(65,40+200-map(contor[1], x, y, 0, 200));
  tft.print(contor[1]);
  tft.setCursor(65,280);
  tft.print(last7[0]);
  
  if(contor[2] > z){
    tft.fillRect(115, 65, 30, 200, RED);
  }else{
      tft.fillRect(115, 65, 30, 200, WHITE);
  }
  tft.fillRect(115, 65, 30, 200-map(contor[2], x, y, 0, 200), BLACK);
    tft.setCursor(120,40+200-map(contor[2], x, y, 0, 200));
  tft.print(contor[2]);
  tft.setCursor(120,280);
  tft.print(last7[1]);

  if(contor[3] > z){
    tft.fillRect(170, 65, 30, 200, RED);
  }else{
      tft.fillRect(170, 65, 30, 200, WHITE);
  }
  tft.fillRect(170, 65, 30, 200-map(contor[3], x, y, 0, 200), BLACK);
  tft.setCursor(175,40+200-map(contor[3], x, y, 0, 200));
  tft.print(contor[3]);
    tft.setCursor(175,280);
  tft.print(last7[2]);

  if(contor[4] > z){
    tft.fillRect(225, 65, 30, 200, RED);
  }else{
      tft.fillRect(225, 65, 30, 200, WHITE);
  }
  tft.fillRect(225, 65, 30, 200-map(contor[4], x, y, 0, 200), BLACK);
  tft.setCursor(230,40+200-map(contor[4], x, y, 0, 200));
  tft.print(contor[4]);
  tft.setCursor(230,280);
  tft.print(last7[3]);

  if(contor[5] > z){
    tft.fillRect(280, 65, 30, 200, RED);
  }else{
      tft.fillRect(280, 65, 30, 200, WHITE);
  }
  tft.fillRect(280, 65, 30, 200-map(contor[5], x, y, 0, 200), BLACK);
  tft.setCursor(285,40+200-map(contor[5], x, y, 0, 200));
  tft.print(contor[5]);
  tft.setCursor(285,280);
  tft.print(last7[4]);

  if(contor[6] > z){
    tft.fillRect(335, 65, 30, 200, RED);
  }else{
      tft.fillRect(335, 65, 30, 200, WHITE);
  }
  tft.fillRect(335, 65, 30, 200-map(contor[6], x, y, 0, 200), BLACK);
  tft.setCursor(340,40+200-map(contor[6], x, y, 0, 200));
  tft.print(contor[6]);
  tft.setCursor(340,280);
  tft.print(last7[5]);

  if(contor[7] > z){
    tft.fillRect(390, 65, 30, 200, RED);
  }else{
      tft.fillRect(390, 65, 30, 200, WHITE);
  }
  tft.fillRect(390, 65, 30, 200-map(contor[7], x, y, 0, 200), BLACK);
  tft.setCursor(395,40+200-map(contor[7], x, y, 0, 200));
  tft.print(contor[7]);
  tft.setCursor(395,280);
  tft.print(last7[6]);
  
   tft.drawLine(0,265,480,265, WHITE);  
}

void Sensors(int input)
{
 tft.setTextColor(BLACK);
 tft.setTextSize(3);
if(input / 1000 % 10 == 1){
  tft.fillRect(0, 65, 480, 50, GREEN);
  tft.setCursor(50, 80);
  tft.println("LIS3DH");
  tft.setCursor(315, 80);
  tft.println("GOOD");
}else{
  tft.fillRect(0, 65, 480, 50, RED);
  tft.setCursor(50, 80);
  tft.println("LIS3DH");
  tft.setCursor(315, 80);
  tft.println("DOWN");
}
if(input / 100 % 10 == 1){
  tft.fillRect(0, 115, 480, 50, GREEN);
  tft.setCursor(50, 130);
  tft.println("MICS");
  tft.setCursor(315, 130);
  tft.println("GOOD");
}else{
  tft.fillRect(0, 115, 480, 50, RED);
  tft.setCursor(50, 130);
  tft.println("MICS");
  tft.setCursor(315, 130);
  tft.println("DOWN");
}
if(input / 10 % 10 == 1){
  tft.fillRect(0, 165, 480, 50, GREEN);
  tft.setCursor(50, 180);
  tft.println("SGP40");
  tft.setCursor(315, 180);
  tft.println("GOOD");
}else{
  tft.fillRect(0, 165, 480, 50, RED);
  tft.setCursor(50, 180);
  tft.println("SGP40");
  tft.setCursor(315, 180);
  tft.println("DOWN");
}
if(input % 10 == 1){
  tft.fillRect(0, 215, 480, 50, GREEN);
  tft.setCursor(50, 230);
  tft.println("BME680");
  tft.setCursor(315, 230);
  tft.println("GOOD");
}else{
  tft.fillRect(0, 215, 480, 50, RED);
  tft.setCursor(50, 230);
  tft.println("BME680");
  tft.setCursor(315, 230);
  tft.println("DOWN");
} 
}


void gettime()
{
  myRTC.updateTime();
  tft.setTextColor(WHITE);
  tft.setTextSize(3.5);
  tft.setCursor(170, 10);
  char buffer[20];
  sprintf(buffer, "%02d/%02d/%04d  %02d:%02d", myRTC.dayofmonth, myRTC.month, myRTC.year, myRTC.hours, myRTC.minutes);

  int currentMinutes = myRTC.minutes;
  int currentHours = myRTC.hours;
  int currentDay = myRTC.dayofmonth;
  int currentMonth = myRTC.month;
  int currentYear = myRTC.year;


  if (currentMinutes != previousMinutes || currentHours != previousHours || currentDay != previousDay || currentMonth != previousMonth || currentYear != previousYear || pageChanged) {
    // Stergerea timpului precedent prin desenarea unui dreptunghi negru
    tft.fillRect(195, 10, 300, 30, BLACK);


    tft.print(buffer); // Afisare timp actualizat

    // Update la valorile precedente
    previousMinutes = currentMinutes;
    previousHours = currentHours;
    previousDay = currentDay;
    previousMonth = currentMonth;
    previousYear = currentYear;
    pageChanged = false;
  }
}

void drawUpdateStatus()
{
  tft.fillScreen(BLACK);
  tft.fillRect(100, 100, 280, 100, WHITE);
  tft.setTextColor(BLACK);
  
  if(UpdateStatus == true){
    tft.setTextSize(2);
    tft.setCursor(137, 140);
    tft.println("Update Successful!");
  }
  if(UpdateStatus == false){
    tft.setTextSize(2);
    tft.setCursor(150, 140);
    tft.println("Update Failed!");
  }
  delay(4000);
  tft.fillScreen(BLACK);
  drawUpdatePage();
}


void drawUpdatePage(){
tft.fillScreen(BLACK);
  Back_btn.drawButton(true);
  WIFI_btn.drawButton(true);
  Time_btn.drawButton(true);
}

void drawPageWIFI(){
  tft.fillScreen(BLACK);
  Back_btn.drawButton(true);

  Enable_btn.drawButton(true);
  Disable_btn.drawButton(true);

  tft.setTextColor(WHITE);
  tft.setTextSize(3);
  tft.setCursor(170, 20);
  tft.println("WIFI STATUS");
  tft.drawLine(0, 55, 480, 55, WHITE);
}


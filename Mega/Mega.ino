#include "Functii.h"
#include "Page_Management.h"
#include "Page_Distribution.h"

const int chipSelect = 53;  //definire pin pentru chip SD+

void setup(void)
{

  uint16_t ID = tft.readID();
  if (ID == 0xD3D3) ID = 0x9486; 
  tft.begin(ID);

  Serial.begin(38400);
  Serial1.begin(38400);


  pinMode(chipSelect, OUTPUT);
  pinMode(controlPin, OUTPUT);

  digitalWrite(controlPin, LOW);

  Buttons_Initialization();

  tft.setRotation(1); // orientare landscape
  tft.fillScreen(BLACK);

    drawPageInitializare();
    delay(2000);

    tft.fillScreen(BLACK);
    drawPageLoading();

    if (!SD.begin()) {
      tft.fillScreen(BLACK);
      drawErrorSD();
    }
      currentPage = PAGEHOME;
  drawHome();
}


void loop(void)
{

page_distribution();
  
}
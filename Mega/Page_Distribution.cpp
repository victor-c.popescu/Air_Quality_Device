#include "Page_Distribution.h"
#include "Functii.h"
#include "Page_Management.h"

int start = 0;
const int controlPin = 45;  //definire pin pentru control Buzzer


void page_distribution()
{  
  bool down = Touch_getXY(&pixel_x, &pixel_y);
  
  switch (currentPage) {

    case PAGEHOME:

      pag1_btn.press(down && pag1_btn.contains(pixel_x, pixel_y));
      
      about_btn.press(down && about_btn.contains(pixel_x, pixel_y));
      istoric_btn.press(down && istoric_btn.contains(pixel_x, pixel_y));

      if (pag1_btn.justPressed()) {
        currentPage = PAGE1;
        tft.fillScreen(BLACK);
        pageChanged = true;
        drawPage1();
      }
      if (istoric_btn.justPressed()) {
        currentPage = PAGE_GRAPHIC;
        tft.fillScreen(BLACK);
        pageChanged = true;
        drawPageGraphics();
      }
      if (about_btn.justPressed()) {
        tft.fillScreen(BLACK);
        pageChanged = true;
        
        currentPage = PAGE_SETTINGS;
        drawPageSettings();
       
      }

      break;

    case PAGE1:

      TVOC_btn.press(down && TVOC_btn.contains(pixel_x, pixel_y));
      Menu_btn.press(down && Menu_btn.contains(pixel_x, pixel_y));
      gettime();
      
      readvalues();

      if (TVOC_btn.justPressed()) {
        currentPage = PAGE2;
        tft.fillScreen(BLACK);
        pageChanged = true;
        drawPage2();
      }
      if (Menu_btn.justPressed()) {
        currentPage = PAGEHOME;
        tft.fillScreen(BLACK);
        pageChanged = true;
        drawHome();
      }

      if (tempFlag == true) {
        tft.setTextColor(BLACK);
        tft.setTextSize(3);
        tft.setCursor(85, 105);
        tft.fillRect(75, 100, 50, 40, TFT_CYAN);
        tft.print(temperatura); 
        tempFlag = false;
      }

      if (presFlag == true) {
        tft.setCursor(70, 225);
        tft.setTextColor(BLACK);
        tft.setTextSize(3);
        tft.fillRect(70, 225, 60, 35, TFT_CYAN);
        tft.print(presiune); 
        presFlag = false;
      }

      if (humFlag == true) {
        tft.setCursor(320, 110); 
        tft.setTextColor(BLACK);
        tft.setTextSize(3);
        tft.fillRect(290, 110, 60, 35, TFT_CYAN);
        tft.print(umiditate); 
        humFlag = false;
      }

      if (eco2Flag == true) {
        tft.setCursor(310, 225);
        tft.setTextColor(BLACK);
        tft.setTextSize(3);
        tft.fillRect(305, 220, 60, 35, TFT_CYAN);
        tft.print(eco2); 
        eco2Flag = false;
      }

      break;

    case PAGE2:

      TPUCO_btn.press(down && TPUCO_btn.contains(pixel_x, pixel_y));
      Menu_btn.press(down && Menu_btn.contains(pixel_x, pixel_y));

      gettime();
      readvalues();

      if (Menu_btn.justPressed()) {
        currentPage = PAGEHOME;
        tft.fillScreen(BLACK);
        pageChanged = true;
        drawHome();
      }
      if (TPUCO_btn.justPressed()) {
        currentPage = PAGE1;
        tft.fillScreen(BLACK);
        pageChanged = true;
        drawPage1();
      }
      
      if (tvocFlag == true) {
        tft.setTextSize(3.5);
        tft.fillRect(70, 120, 60, 35, TFT_CYAN);
        tft.setTextColor(BLACK);
        tft.setCursor(70, 130);
        tft.print(tvoc); 
        tft.fillRect(150, 250, 30, (-1)*map(tvoc, 0, 2000, 0, 200), GREEN);
        tft.fillRect(150, 50, 30, 200 - map(tvoc, 0, 2000, 0, 200), WHITE);
        tvocFlag = false;
      }
      if (pm10Flag == true) {
        tft.setTextSize(3.5);
        tft.fillRect(260, 100, 80, 35, TFT_CYAN);
        tft.setTextColor(BLACK);
        tft.setCursor(260, 100);
        tft.print(pm10);
        pm10Flag = false;
      }
      if (pm25Flag == true) {
        tft.setTextSize(3.5);
        tft.fillRect(260, 220, 80, 35, TFT_CYAN);
        tft.setTextColor(BLACK);
        tft.setCursor(260, 220);
        tft.print(pm25);
        pm25Flag = false;
      }

      break;

    case PAGE_GRAPHIC:

      Menu_btn.press(down && Menu_btn.contains(pixel_x, pixel_y));
      Temperature_Graph_btn.press(down && Temperature_Graph_btn.contains(pixel_x, pixel_y));
      Humidity_Graph_btn.press(down && Humidity_Graph_btn.contains(pixel_x, pixel_y));
      Eco2_Graph_btn.press(down && Eco2_Graph_btn.contains(pixel_x, pixel_y));
      TVOC_Graph_btn.press(down && TVOC_Graph_btn.contains(pixel_x, pixel_y));
      Back_to_GraphPage_btn.press(down && Back_to_GraphPage_btn.contains(pixel_x, pixel_y));
      if (Menu_btn.justPressed()) {
        currentPage = PAGEHOME;
        tft.fillScreen(BLACK);
        pageChanged = true;
        drawHome();
      }
      if (Temperature_Graph_btn.justPressed()) {
        currentPage = PAGE_TEMP_GRAPH;
        tft.fillScreen(BLACK);
        pageChanged = true;

      }
      if (Humidity_Graph_btn.justPressed()) {
        currentPage = PAGE_HUM_GRAPH;
        tft.fillScreen(BLACK);
        pageChanged = true;

      }
      if (Eco2_Graph_btn.isPressed()) {
        currentPage = PAGE_Eco2_GRAPH;
        tft.fillScreen(BLACK);
        pageChanged = true;

      }
      if(Back_to_GraphPage_btn.justPressed()){
        currentPage = PAGE_GRAPHIC;
        tft.fillScreen(BLACK);
        pageChanged = true;
        drawPageGraphics();
      }
      if (TVOC_Graph_btn.justPressed()) {
        currentPage = PAGE_TVOC_GRAPH;
        tft.fillScreen(BLACK);
        pageChanged = true;

      }

      break;

    case PAGE_TEMP_GRAPH:
      currentPage = PAGE_GRAPHIC;
      read_lines(Fisier_Temperatura, contor);
      tft.fillScreen(BLACK);
      tft.setTextColor(WHITE);
      tft.setTextSize(3);
      tft.setCursor(155,20);
      tft.print("Temperatura (*C)");
      drawGrafic(0,100,30);


      break;

    case PAGE_HUM_GRAPH:

      currentPage = PAGE_GRAPHIC;
      read_lines(Fisier_Umiditate, contor);
      tft.fillScreen(BLACK);
      tft.setTextColor(WHITE);
      tft.setTextSize(3);
      tft.setCursor(150, 20);
      tft.println("Umiditate (%)");
      drawGrafic(0,100,65);
      break;

    case PAGE_Eco2_GRAPH:

      currentPage = PAGE_GRAPHIC;
      read_lines(Fisier_Eco2, contor);
      tft.fillScreen(BLACK);
      tft.setTextColor(WHITE);
      tft.setTextSize(3);
      tft.setCursor(170, 20);
      tft.println("Eco2 (ppm)");
      drawGrafic(0,5000,900);
      break;

    case PAGE_TVOC_GRAPH:

      currentPage = PAGE_GRAPHIC;
      read_lines(Fisier_TVOC, contor);
      tft.fillScreen(BLACK);
      tft.setTextColor(WHITE);
      tft.setTextSize(3);
      tft.setCursor(250, 20);
      tft.println("TVOC");
      drawGrafic(0,1000,200);
      break;

    case PAGELEAKING:
      stop_btn.press(down && stop_btn.contains(pixel_x, pixel_y));
      digitalWrite(controlPin, HIGH);
      if (stop_btn.justPressed()) {
        Serial1.println("stop");
        digitalWrite(controlPin, LOW);
        currentPage = PAGE1;
        tft.fillScreen(BLACK);
        drawPage1();
      }

      break;

      case PAGE_SETTINGS:
      Menu_btn.press(down && Menu_btn.contains(pixel_x, pixel_y));
      UpdateTime_btn.press(down && UpdateTime_btn.contains(pixel_x, pixel_y));
      Check_btn.press(down && Check_btn.contains(pixel_x, pixel_y));
      Check_Hardware_btn.press(down && Check_Hardware_btn.contains(pixel_x, pixel_y));
        if (Menu_btn.justPressed()) {
        currentPage = PAGEHOME;
        tft.fillScreen(BLACK);
        pageChanged = true;
        drawHome();
      }if (Check_btn.justPressed()) {
        Serial1.println("*qsa");
        drawCheckSystem();
        currentPage = PAGE_CHECK_SYSTEM;
        pageChanged = true;
      }
      if(UpdateTime_btn.justPressed()){
       drawUpdatePage();
       currentPage = PAGE_UPDATE;
      pageChanged = true;
      }
      if(Check_Hardware_btn.justPressed()){
        tft.fillScreen(BLACK);
        currentPage = PAGE_CHECK_HARDWARE;
        Serial1.println("w1");
        pageChanged = true;
        drawCheckHardware();
      }

      
      break;

      case PAGE_UPDATE:
      Back_btn.press(down && Back_btn.contains(pixel_x, pixel_y));
      Time_btn.press(down && Time_btn.contains(pixel_x, pixel_y));
      WIFI_btn.press(down && WIFI_btn.contains(pixel_x, pixel_y));
      sent_wifi_request_flag = false;
      ip_print_flag = false;

      if (Back_btn.justPressed()) {
         tft.fillScreen(BLACK);
         delay(1000);
        pageChanged = true;
        currentPage = PAGE_SETTINGS;
        drawPageSettings();
      }
      if (Time_btn.justPressed()) {
        UpdateTime_FromFile();
        drawUpdateStatus();
      }
      if (WIFI_btn.justPressed()) {
         tft.fillScreen(BLACK);
        pageChanged = true;
        currentPage = PAGE_WIFI;
        drawPageWIFI();
      }

      break;

      case PAGE_WIFI:
      Back_btn.press(down && Back_btn.contains(pixel_x, pixel_y));
      Enable_btn.press(down && Enable_btn.contains(pixel_x, pixel_y));
      Disable_btn.press(down && Disable_btn.contains(pixel_x, pixel_y));
 
      if(sent_wifi_request_flag == false){
        Serial1.println("!1");
        sent_wifi_request_flag = true;
        readWIFI();
      }
      readWIFI();

      if(wifiFlag == false && errorFlag == false){
      tft.setTextColor(WHITE);
      tft.setTextSize(3);
      tft.setCursor(150, 100);
      tft.println("Not Connected");
      }

      if (Enable_btn.justPressed() && wifiFlag == false){
        wifiFlag = true;
        ip_print_flag = false;
        readSSIDAndPassword();
        tft.fillRect(100, 90, 300, 100, BLACK);
        Serial1.println(combined);
        Serial.println(combined);
        tft.setTextColor(WHITE);
        tft.setTextSize(3);
        tft.setCursor(50, 100);
        tft.println("Attempting to connect");
      }
        if (Disable_btn.justPressed() && wifiFlag == true){
        wifiFlag = false;
        ip_print_flag = false;
        Serial1.println("^1");
        tft.fillRect(70, 90, 400, 100, BLACK);
        tft.setTextColor(WHITE);
        tft.setTextSize(3);
        tft.setCursor(150, 100);
        tft.println("Not Connected");
        ip_adress = "";
      }
      if(ip_adress.length() > 1 && ip_print_flag == false && wifiFlag == true && errorFlag == false){
        ip_print_flag = true;
        tft.fillRect(40, 90, 450, 100, BLACK);
        tft.setCursor(150, 100);
        tft.println("Connected");
        tft.setCursor(80, 150);
        tft.println("IP: " + ip_adress);
      }
      if(errorFlag == true && ip_print_flag == false){
        ip_print_flag = true;
        wifiFlag = false;
        tft.fillRect(40, 90, 450, 100, BLACK);
        tft.setCursor(200, 100);
        tft.println("Error");
      }

            if (Back_btn.justPressed()) {
         tft.fillScreen(BLACK);
        pageChanged = true;
        currentPage = PAGE_SETTINGS;
        drawPageSettings();
      }
      break;

      case PAGE_CHECK_SYSTEM:
      Menu_btn.press(down && Menu_btn.contains(pixel_x, pixel_y));
      Check_SD_btn.press(down && Check_SD_btn.contains(pixel_x, pixel_y));
    
      readstatus();
      if (Menu_btn.justPressed()) {
        currentPage = PAGEHOME;
        tft.fillScreen(BLACK);
        pageChanged = true;
        drawHome();
      }
      if(Check_SD_btn.justPressed()){
        drawCheckSD();
        currentPage = PAGE_CHECK_SD;
        pageChanged = true;
      }
      
      break;

      case PAGE_CHECK_SD:
      Menu_btn.press(down && Menu_btn.contains(pixel_x, pixel_y));
      Back_to_Check_btn.press(down && Back_to_Check_btn.contains(pixel_x, pixel_y));
       if (Menu_btn.justPressed()) {
        currentPage = PAGEHOME;
        tft.fillScreen(BLACK);
        pageChanged = true;
        drawHome();
      }
      if(Back_to_Check_btn.justPressed()){
        Serial1.println("*qsa");
        drawCheckSystem();
        currentPage = PAGE_CHECK_SYSTEM;
        pageChanged = true;  
      }

      case PAGE_CHECK_HARDWARE:
      Menu_btn.press(down && Menu_btn.contains(pixel_x, pixel_y));
      Buzzer_Start_btn.press(down && Buzzer_Start_btn.contains(pixel_x, pixel_y));
      Buzzer_Stop_btn.press(down && Buzzer_Stop_btn.contains(pixel_x, pixel_y));
      Power_Trigger_Start_btn.press(down && Power_Trigger_Start_btn.contains(pixel_x, pixel_y));
      Power_Trigger_Stop_btn.press(down && Power_Trigger_Stop_btn.contains(pixel_x, pixel_y));
      Gas_Trigger_Start_btn.press(down && Gas_Trigger_Start_btn.contains(pixel_x, pixel_y));
      Gas_Trigger_Stop_btn.press(down && Gas_Trigger_Stop_btn.contains(pixel_x, pixel_y));
      Air_Vent_Start_btn.press(down && Air_Vent_Start_btn.contains(pixel_x, pixel_y));
      Air_Vent_Stop_btn.press(down && Air_Vent_Stop_btn.contains(pixel_x, pixel_y));
       if (Menu_btn.justPressed()) {
        currentPage = PAGEHOME;
        Serial1.println("x1");
        tft.fillScreen(BLACK);
        pageChanged = true;
        drawHome();
      }
      if(Buzzer_Start_btn.justPressed()){
        digitalWrite(controlPin, HIGH);
        Serial1.println("b0");
      }
      if(Buzzer_Stop_btn.justPressed()){
        digitalWrite(controlPin, LOW);
        Serial1.println("b1");
      }
      if(Power_Trigger_Start_btn.justPressed()){
        Serial1.println("p0");
      }
      if(Power_Trigger_Stop_btn.justPressed()){
        Serial1.println("p1");
      }
      if(Gas_Trigger_Start_btn.justPressed()){
        Serial1.println("g0");
      }
      if(Gas_Trigger_Stop_btn.justPressed()){
        Serial1.println("g1");
      }
      if(Air_Vent_Start_btn.justPressed()){
        Serial1.println("a0");
      }
      if(Air_Vent_Stop_btn.justPressed()){
        Serial1.println("a1");
      }


      break;
    default:
  
      break;
  }
  
  
}

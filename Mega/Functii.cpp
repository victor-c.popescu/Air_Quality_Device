#include "Functii.h"
#include "Page_Management.h"

#define MINPRESSURE 200                       //definire presiune minima pentru apasarea pe touchscreen
#define MAXPRESSURE 1000                      //definire presiune maxima pentru apasarea pe touchscreen

File myFile;  //creare fisier myFile
virtuabotixRTC myRTC(31, 33, 35);  //creare obiect myRTC pentru modulul de timp - pini = data/clock/reset


const int XP = 8, XM = A2, YP = A3, YM = 9; //320x480 ID=0x9488 (definire pini pentru touchscreen)
const int TS_LEFT = 194, TS_RT = 919, TS_TOP = 952, TS_BOT = 195; //definire constante pentru calibrare touchscreen

TouchScreen ts = TouchScreen(XP, YP, XM, YM, 300); //creare obiect ts petntru touchscreen

int pixel_x, pixel_y;     
int temperatura, umiditate, presiune, eco2, tvoc;
String dust="";
int previousTemperature = -1;
int previousHumidity = -1;
int previousPressure = -1;
int previousEco2 = -1;
int previousDust = -1;
int previousTvoc = -1;
float previouspm10 = -1.00;
float previouspm25 = -1.00;
int sensor_status;
float pm10,pm25;
int val[11];  //vector pentru stocarea valorilor citite din fisiere
int cnt[11]; //vector pentru contorizarea numarului de valori citire din fisier
int ok = 0;
const int numLines = 200;

uint64_t curent_time;
uint64_t valoare;

bool tempFlag = false;
bool presFlag = false;
bool humFlag = false;
bool eco2Flag = false;
bool dustFlag = false;
bool tvocFlag = false;
bool pm10Flag = false;
bool pm25Flag = false;
bool UpdateStatus = false;
bool pageChanged = false;
bool wifiFlag = false;
bool errorFlag = false;
bool ip_print_flag = false;
bool sent_wifi_request_flag = false;

//definire valori pentru stocarea milisecundelor in cazul necesitarii scrierii in fisiere odata la 10 min
unsigned long previousMillis_tmp = 0;
unsigned long previousMillis_eco2 = 0;
unsigned long previousMillis_hum = 0;
unsigned long previousMillis_tvoc = 0;
unsigned long interval = 60000UL;

//definire stringuri pentru identificare fisiere
String Fisier_Temperatura = "Temp.txt";
String Fisier_Umiditate = "Umid.txt";
String Fisier_Eco2 = "Eco2.txt";
String Fisier_TVOC = "TVOC.txt";
String Fisier_Time = "Time.txt";
String combined ="";
String ip_adress = "";



uint64_t cutStringIntoWords(String inputString) {
  int totalChars = inputString.length();
  int startPos = 0;
  int wordLengths[] = {2, 2, 4, 2, 2}; //definire numar caractere ale fiecari bucati din data ex:17/01/2024 22:15
  int daysInMonth[] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}; //Nr de zile din fiecare luna


  int day, month, year, hour, minute;

  // Parcurgere vector cu lungimile cuvintelor
  for (int i = 0; i < sizeof(wordLengths) / sizeof(wordLengths[0]); ++i) {
    // Se trece peste spatii
    while (startPos < totalChars && inputString[startPos] == ' ') {
      startPos++;
    }
    // Verificare daca se poate extrage intregul cuvant
    if (startPos + wordLengths[i] <= totalChars) {
      // Se extrage cuvantul curent
      String currentWord = inputString.substring(startPos, startPos + wordLengths[i]);

      //Stocare si convertire cuvant in intreg
      if (i == 0) {
        day = currentWord.toInt();
      } else if (i == 1) {
        month = currentWord.toInt();
      } else if (i == 2) {
        year = currentWord.toInt();
      } else if (i == 3) {
        hour = currentWord.toInt();
      } else if (i == 4) {
        minute = currentWord.toInt();
      }

      // Se actualizează poziția de început pentru următorul cuvânt și se adaugă 1 pentru a omite separatorul
      startPos += wordLengths[i] + 1;
    } else {
      break; // Ieșirea din buclă dacă șirul nu conține destul de multe caractere pentru extragerea cuvântului curent
    }
  }
  // Se calculează un număr care reprezinta data calculata in minute fara luna
  uint64_t nr = minute + hour * 60ULL + day * 24 * 60ULL;

  
  //adunam zilele din fiecare luna pana in luna curenta
    for (int i = 1; i < month; i++) {
    nr += daysInMonth[i] * 24 * 60ULL;
  }
  
  //Verificam daca este an bisect
  if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0 && year % 100 == 0 && year % 4 == 0)) {
    nr += 24 * 60ULL; // Adunam o zi in plus daca este
  }

  return nr; 
}

int extractValueAfterArrow(String inputString) {

  int arrowPos = inputString.indexOf("->"); //Se găsește poziția simbolului "->" în șir

  //Se verifică dacă simbolul a fost găsit
  if (arrowPos != -1) {
    int startPos = arrowPos + 2;  //Se determină poziția de început a valorii după "->"

    //Se sare peste spațiile existente
    while (startPos < inputString.length() && inputString[startPos] == ' ') {
      startPos++;
    }


    String valueString = inputString.substring(startPos); //Se extrage partea din șir care conține valoarea


    int value = valueString.toInt(); //Se convertește șirul într-un număr întreg
    return value; 

  } else {
    return;
  }
}

void write_file(int value, String Fisier) {
  if (!SD.begin()) {
    return;
  }

  myRTC.updateTime();
  String the_time = String(myRTC.dayofmonth) + "/" + String(myRTC.month) + "/" + String(myRTC.year) + " " + String(myRTC.hours) + ":" + String(myRTC.minutes) + " -> " + String(value);
  myFile = SD.open(Fisier, FILE_WRITE);
  if (myFile) {
    myFile.println(the_time);
    myFile.close();
  } else {
    return;
  }
}


uint64_t cuttime()
{
  myRTC.updateTime();
  uint64_t nr1 = myRTC.minutes + myRTC.hours * 60ULL + myRTC.dayofmonth * 24 * 60ULL + (myRTC.month -1) * 30 * 24 * 60ULL;
  return nr1;
}

String readSSIDAndPassword() {
  
  File myFile = SD.open("network.txt");
  
  
  if (!myFile) {
    Serial.println("Error opening file!");
    return "";
  }
  
 
  String ssid = myFile.readStringUntil('\n');
  ssid.trim(); // Indepartare spatii libere sau charactere newline


  String password = myFile.readStringUntil('\n');
  password.trim();  // Indepartare spatii libere sau charactere newline

  myFile.close();
  
   combined = "%" + ssid + " " + password;
  return combined;
}




void UpdateTime_FromFile(){
  int wordLengths[] = {2, 2, 4, 2, 2}; //definire numar caractere ale fiecari bucati din data ex:17/01/2024 22:15
  String content;
  int day, month, year, hour, minute;
  int startPos = 0;
  //Se verifică dacă cardul SD poate fi inițializat cu succes
  if (!SD.begin()) {
    UpdateStatus = false;
  }

  File myFile = SD.open("Time.txt", FILE_READ); //Se deschide fișierul pentru citire
  
  if (myFile) {
    // Verificare dacă fișierul nu este gol
    if (myFile.size() > 0) {
     content = myFile.readString();
     

  }
   myFile.close();
  }
  int totalChars = content.length();

    for (int i = 0; i < sizeof(wordLengths) / sizeof(wordLengths[0]); ++i) {
    // Se trece peste spatii
    while (startPos < totalChars && content[startPos] == ' ') {
      startPos++;
    }
    // Verificare daca se poate extrage intregul cuvant
    if (startPos + wordLengths[i] <= totalChars) {
      // Se extrage cuvantul curent
      String currentWord = content.substring(startPos, startPos + wordLengths[i]);

      //Stocare si convertire cuvant in intreg
      if (i == 0) {
        day = currentWord.toInt();
      } else if (i == 1) {
        month = currentWord.toInt();
      } else if (i == 2) {
        year = currentWord.toInt();
      } else if (i == 3) {
        hour = currentWord.toInt();
      } else if (i == 4) {
        minute = currentWord.toInt();
      }
UpdateStatus = true;
      // Se actualizează poziția de început pentru următorul cuvânt și se adaugă 1 pentru a omite separatorul
      startPos += wordLengths[i] + 1;
    
  
    } else {
      break; // Ieșirea din buclă dacă șirul nu conține destul de multe caractere pentru extragerea cuvântului curent
    }
  }

  myRTC.setDS1302Time(10, minute, hour, 7, day, month, year);
  
}

void readvalues() {
  if (Serial1.available()) {

    String receivedData = Serial1.readStringUntil('\n');
    


    if (receivedData.length() > 0) {
      char firstLetter = receivedData.charAt(0);  // Gaseste primul caracter
      char fourthLetter = receivedData.charAt(3); // Gaseste al patrulea caracter

      if (firstLetter == 'T' && fourthLetter == 'p') {
        receivedData = receivedData.substring(7);
        temperatura = receivedData.toInt();
        if (millis() - previousMillis_tmp > interval)
        {
          previousMillis_tmp += interval;
          write_file(temperatura, Fisier_Temperatura);
        }

        if (temperatura != previousTemperature) {
          previousTemperature = temperatura;
          tempFlag = true;
        }

      } else if (firstLetter == 'P') {
        receivedData = receivedData.substring(4);
        presiune = receivedData.toInt();


        if (presiune != previousPressure) {
          previousPressure = presiune;
          presFlag = true;
        }

      } else if (firstLetter == 'U') {
        receivedData = receivedData.substring(4);
        umiditate = receivedData.toInt();
        if (millis() - previousMillis_hum > interval)
        {
          previousMillis_hum += interval;
          write_file(umiditate, Fisier_Umiditate);
        }

        if (umiditate != previousHumidity) {
          previousHumidity = umiditate;
          humFlag = true;
        }

      } else if (firstLetter == 'E') {
        receivedData = receivedData.substring(4);
        eco2 = receivedData.toInt();

        if (millis() - previousMillis_eco2 > interval)
        {
          previousMillis_eco2 += interval;
          write_file(eco2, Fisier_Eco2);
        }

        if (eco2 != previousEco2) {
          previousEco2 = eco2;
          eco2Flag = true;
        }

      } else if (firstLetter == '@') {
    
        receivedData = receivedData.substring(1);
        tft.fillScreen(BLACK);
        currentPage = PAGELEAKING;
        drawPageLeaking();

      } else if (firstLetter == 'c') {
        
        receivedData = receivedData.substring(1); 
        dust = receivedData;
        int hyphenIndex = dust.indexOf('-');
        String pm10String = dust.substring(0, hyphenIndex);
        pm10 = pm10String.toFloat();

        String pm25String = dust.substring(hyphenIndex + 1);
        pm25 = pm25String.toFloat();

        
        if (pm10 != previouspm10) {
          previouspm10 = pm10;
          pm10Flag = true;
        }
         if (pm25 != previouspm25) {
          previouspm25 = pm25;
          pm25Flag = true;
        }
        
      } else if (firstLetter == 'T' && fourthLetter == 'C') {
        receivedData = receivedData.substring(7);
        tvoc = receivedData.toInt();

         if (millis() - previousMillis_tvoc > interval)
        {
          previousMillis_tvoc += interval;
          write_file(tvoc, Fisier_TVOC);
        }

        if (tvoc != previousTvoc) {
          previousTvoc = tvoc;
          tvocFlag = true;

        }
      }else if(firstLetter == '#'){
       receivedData = receivedData.substring(1);
       sensor_status = receivedData.toInt();
       if(sensor_status != 1111){
       Serial1.println("*qsa");
       drawCheckSystem();
           currentPage = PAGE_CHECK_SYSTEM;
           pageChanged = true;
    }
    }
    }
  }
}

void readWIFI(){
       if (Serial1.available()) {


    String receivedData = Serial1.readStringUntil('\n');

    if (receivedData.length() > 0) {
      char firstLetter = receivedData.charAt(0);  // Gaseste primul caracter

 if(firstLetter == '$'){
      receivedData = receivedData.substring(1);
      ip_adress = receivedData;
      Serial.println(ip_adress);
    }else if (firstLetter == '('){
      errorFlag = false;
      wifiFlag = true;
      receivedData = receivedData.substring(1);
      ip_adress = receivedData;
     
    }else if (firstLetter == ')'){
      errorFlag = false;
      wifiFlag = false;
      
    }
    else if (firstLetter == '~'){
      errorFlag = true;
      
    }
}
       }
}
void readstatus()
{
  if (Serial1.available()) {

    String receivedData = Serial1.readStringUntil('\n');
    if (receivedData.length() > 0) {
      char firstLetter = receivedData.charAt(0);  // Gaseste primul caracter

    if(firstLetter == '#'){
       receivedData = receivedData.substring(1);
       sensor_status = receivedData.toInt();
       Sensors(sensor_status);
    }
    }
  }
}


int get_last_7_days(int previousDay[7]) {
  int cnt_day = 0;
  int remaining_days;
  int currentDay = myRTC.dayofmonth;
  int currentMonth = myRTC.month;
  int daysInMonth[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
 
  if (currentDay < 8) {

    for (int i = 0; i < 7; i++) {
      if (previousDay[i - 1] == 1 || currentDay == 1) {
        break;
      }
      previousDay[i] = currentDay - i - 1;
      cnt_day++;
    }
    remaining_days = 7 - cnt_day;
    for (int i = 1 ; i < remaining_days + 1; i++) {
      previousDay[cnt_day + i - 1] = daysInMonth[currentMonth - 2] - i + 1;
    }
  } else {
    for (int i = 0; i < 7; i++) {
      previousDay[i] = currentDay - i - 1;
    }
  }

  return previousDay;
}

void read_lines(String FileToRead, int cont[])
{
  //Se verifică dacă cardul SD poate fi inițializat cu succes
  if (!SD.begin()) {
    Serial.println("Initialization failed!");
    return;
  }

  File myFile = SD.open(FileToRead, FILE_READ); //Se deschide fișierul pentru citire

  // Verificare dacă fișierul s-a deschis cu succes
  if (myFile) {
    // Verificare dacă fișierul nu este gol
    if (myFile.size() > 0) {

      ok = 0;

      if (ok == 0) {
        myFile.seek(myFile.size());
        ok = 1;
      }

      curent_time = cuttime(); //Se bține timpul curent

      //Se inițializează vectorii
      for (int j = 1; j <= 10; j++) {
        cont[j] = 0;
        cnt[j] = 0;
        val[j] = 0;
      }
      //Se parcurge fiecare linie a fișierului
      for (int i = 0; i < numLines; ++i) {
        int q = 0;
        //Se mută cursorul înapoi până când se găsește un caracter \n
        while (myFile.position() > 0 && myFile.read() != '\n') {
          myFile.seek(myFile.position() - 2);
        }


        String currentLine = "";
        while (myFile.available() && q == 0) {
          char c = myFile.read();
          //Se verifică dacă s-a găsit un caracter nou de linie
          if (c == '\n') {
            q = 1;

          }
          currentLine += c;
        }
        String line = "";
        line = currentLine.substring(0, currentLine.length() - 1); //Se elimina \n de la finalul liniei citite
        valoare = cutStringIntoWords(line); //Se extrage valoarea pentru numarul de ore al datei la care a fost scrisa linia
        curent_time = cuttime(); //Se obtine valoarea pentru numarul de ore al datei actuale

        //Se parcurge vectoul pentru stocarea valorilor corespunzatoare ultimelor 10 zile
        for (int j = 1; j <= 10; j++) {
          //Daca valoarea extrasa se incadreaza in intervalul corespunzator (1440 inseamna o zi in ore)
          if (curent_time - 1440 * (j - 1) > valoare && valoare > curent_time - 1440 * j) {
            val[j] = extractValueAfterArrow(line); //Se stocheaza valoarea in vectorul val
            cont[j] = cont[j] + val[j]; //Se actualizeaza suma din vectorul contor cu valoarea din val
            cnt[j]++; //Se incrementeaza counterul pentru numarul de valori citite
          }
        }




        myFile.seek(myFile.position() - currentLine.length() - 2); // mutare cursor la inceputul urmatoarei linii


        // daca am ajuns la finalul fisierului
        if (myFile.position() == 21) {
          break;
        }
      }

      myFile.seek(myFile.size());//Nu mai stiu daca mai e nevoie de linia asta sau nu :)
      myFile.close();
    } else {
      Serial.println("File is empty");
    }

    myFile.close();
    for (int j = 1; j <= 10; j++) {
      cont[j] = cont[j] / cnt[j]; //Se modifica vectorul contor din suma vlaorilor in media lor
    }
return cont;

  } else {
    Serial.println("Failed to open file");
  }
}

bool Touch_getXY(int *pixel_x, int *pixel_y)
{
  TSPoint p = ts.getPoint();
  pinMode(YP, OUTPUT);      
  pinMode(XM, OUTPUT);
  digitalWrite(YP, HIGH);   
  digitalWrite(XM, HIGH);
  bool pressed = (p.z > MINPRESSURE && p.z < MAXPRESSURE);
  if (pressed) {
    *pixel_x = map(p.y, 952, 95, 0, 480);
    *pixel_y = map(p.x, 919, 194, 0, 320);
  }
  return pressed;
}


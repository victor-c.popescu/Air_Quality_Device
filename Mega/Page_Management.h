#ifndef PAGE_MANAGEMENT_H
#define PAGE_MANAGEMENT_H

#include <MCUFRIEND_kbv.h>
#include <Adafruit_GFX.h>
#include <Adafruit_TFTLCD.h>

#define BLACK   0x0000                        
#define BLUE    0x001F
#define RED     0xF800
#define GREEN   0x03E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF
#define BROWN   0x2014



enum Page {
  PAGE1,
  PAGE2,
  PAGE_GRAPHIC,
  PAGEINITIALIZARE,
  PAGELEAKING,
  PAGELOADING,
  PAGEHOME,
  PAGE_TEMP_GRAPH,
  PAGE_HUM_GRAPH,
  PAGE_Eco2_GRAPH,
  PAGE_TVOC_GRAPH,
  PAGE_ERROR_SD,
  PAGE_SETTINGS,
  PAGE_UPDATE_STATUS,
  PAGE_CHECK_SYSTEM,
  PAGE_CHECK_SD,
  PAGE_CHECK_HARDWARE,
  PAGE_UPDATE,
  PAGE_WIFI
};

extern MCUFRIEND_kbv tft;  //creare obiect tft pentru controlul lcd-ului
extern Page currentPage; 

extern Adafruit_GFX_Button TVOC_btn;
extern Adafruit_GFX_Button TPUCO_btn;
extern Adafruit_GFX_Button stop_btn;
extern Adafruit_GFX_Button pag1_btn;
extern Adafruit_GFX_Button istoric_btn;
extern Adafruit_GFX_Button about_btn;
extern Adafruit_GFX_Button Menu_btn;
extern Adafruit_GFX_Button Temperature_Graph_btn;
extern Adafruit_GFX_Button Humidity_Graph_btn;
extern Adafruit_GFX_Button Eco2_Graph_btn;
extern Adafruit_GFX_Button TVOC_Graph_btn;
extern Adafruit_GFX_Button Back_to_GraphPage_btn;
extern Adafruit_GFX_Button UpdateTime_btn;
extern Adafruit_GFX_Button Check_btn;
extern Adafruit_GFX_Button Check_SD_btn;
extern Adafruit_GFX_Button Back_to_Check_btn;
extern Adafruit_GFX_Button Check_Hardware_btn;
extern Adafruit_GFX_Button Buzzer_Start_btn;
extern Adafruit_GFX_Button Buzzer_Stop_btn;
extern Adafruit_GFX_Button Power_Trigger_Start_btn;
extern Adafruit_GFX_Button Power_Trigger_Stop_btn;
extern Adafruit_GFX_Button Gas_Trigger_Start_btn;
extern Adafruit_GFX_Button Gas_Trigger_Stop_btn;
extern Adafruit_GFX_Button Air_Vent_Start_btn;
extern Adafruit_GFX_Button Air_Vent_Stop_btn;
extern Adafruit_GFX_Button WIFI_btn;
extern Adafruit_GFX_Button Time_btn;
extern Adafruit_GFX_Button Back_btn;
extern Adafruit_GFX_Button Enable_btn;
extern Adafruit_GFX_Button Disable_btn;

extern int contor[11];
extern int col[8];

void Buttons_Initialization();

void drawPageSettings();

void drawCheckHardware();

void drawCheckSystem();

void drawCheckSD();

void drawPage1();

void drawPage2();

void drawPageGraphics();

void drawErrorSD();

void drawPageInitializare();

void drawPageLeaking();

void drawHome();

void drawPageLoading();

void checkSD();

void drawGrafic(int x, int y, int z);

void Sensors(int input);

void gettime();

void drawUpdateStatus();

void drawUpdatePage();

void drawPageWIFI();

#endif
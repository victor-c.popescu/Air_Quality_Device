#include <Wire.h>
#include <KarserDSM501.h>

#define DUST_SENSOR_PIN_PM10  3  
#define DUST_SENSOR_PIN_PM25  2  

#define INTERVAL_COUNTDOWN 1000
#define INTERVAL_READ 30000

void pm10_handleInterrupt();
void pm25_handleInterrupt();

KarserDSM501 pm10(DUST_SENSOR_PIN_PM10, pm10_handleInterrupt);
KarserDSM501 pm25(DUST_SENSOR_PIN_PM25, pm25_handleInterrupt);

void pm10_handleInterrupt() { pm10.handleInterrupt(); }
void pm25_handleInterrupt() { pm25.handleInterrupt(); }
String x="";
String y="";
unsigned long timer = 0;

void setup() {
  Serial.begin(9600);
  Wire.begin(); 
}

void loop() {
  
  if (!pm10.isReady() && (millis() >= timer + INTERVAL_COUNTDOWN)) {
 
    timer = timer + INTERVAL_COUNTDOWN;
  } else if (millis() >= timer + INTERVAL_READ) {
    timer = timer + INTERVAL_READ;
      x=String(pm10.readPM());
      y=String(pm25.readPM());

  Wire.beginTransmission(9); 
  Wire.write(y.c_str(), y.length());
  Wire.endTransmission();
  
  }
  delay(500);
}
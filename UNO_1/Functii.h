#ifndef FUNCTII_H
#define FUNCTII_H


#include <Adafruit_Sensor.h>
#include <Adafruit_LIS3DH.h>
#include "Adafruit_SGP30.h"
#include "Adafruit_BME680.h"
#include "DFRobot_MICS.h"
#include <Servo.h>
#include <WiFiS3.h>


#define LED_Earthquake 2
#define LED_Gas A2 

#define DSM501_WARMUP_TIME 30000 


extern Adafruit_BME680 bme;
extern Adafruit_SGP30 sgp;
extern DFRobot_MICS_ADC mics;
extern Adafruit_LIS3DH lis;
extern Servo myservo;



extern int alarma;
extern int Relaypin;
extern int Relaypin2;
extern int status;
extern const int controlPin;
extern const char button_pin_earthquake;
extern const char button_pin_gas;
extern const byte BTpin;
extern int redPin;
extern int greenPin;
extern int bluePin;
extern WiFiServer server;
extern unsigned long startTime;
extern bool earthquake_flag;
extern bool gas_flag;
extern int ec, tvc, hum, pres, temp, index;
extern float pm10,pm25;
extern String receivedString;

void receiveEvent(int howMany);

void check_for_nano();

void setColor(int redValue, int greenValue, int blueValue);

void check_RGB();

void check_buttons();

int calculateAverage();

void checkNext20Values(int upperLimit, int lowerLimit);

void check_earthquake();

void verifySensors();

void check_for_gas();

void splitString();

void check_for_serial();

void time_interval();

void start_alarms();

void stop_alarms();

void gas();

void handleWebClient(WiFiClient client);

#endif

#include "Functii.h"


Adafruit_BME680 bme(&Wire1);
Adafruit_SGP30 sgp;
Adafruit_LIS3DH lis = Adafruit_LIS3DH(&Wire1);
#define ADC_PIN A0
#define POWER_PIN 7


Servo myservo;

int redPin= 13;
int greenPin = 6;
int bluePin = 5;


int Gas_CH4 = 0;
int Gas_CO = 0;
int Gas_C2H5OH = 0;
int Gas_C3H8 = 0;
int Gas_C4H10 = 0;
int Gas_H2 = 0;
int Gas_H2S = 0;
int Gas_NH3 = 0;
int alarma = 0;
int Relaypin = 10;
int Relaypin2 = 11;
int servo_start = 0;
int pos = 45;
int bucla = 0;
int ec, tvc, hum, pres, temp, index;
float pm10,pm25;
unsigned long startTime;
const unsigned long hoursToMillis = 6 * 60 * 60 * 1000; // 6 hours in milliseconds
//const unsigned long hoursToMillis = 180000;  // 3 min in milliseconds
String receivedString = "";

const int controlPin = 4;
const char button_pin_earthquake = 3;
const char button_pin_gas = 12;
const byte BTpin = 8;

bool write_to_mega = false;
bool earthquake = false;
bool led_earthquake_flag = true;
bool led_gas_flag = true;
bool pressed = false;
bool earthquake_flag = true;
bool gas_flag = true;


int avg;
float x, y, tot,z;
int offset =20;
int volt;
double voltage;
String value1 ="";
String value2 ="";
String IP = "";

int status = WL_IDLE_STATUS;
WiFiServer server(80);


DFRobot_MICS_ADC mics(/*adcPin*/ ADC_PIN, /*powerPin*/ POWER_PIN);

void receiveEvent(int howMany) {
   while (Wire.available()) {
    char c = Wire.read(); 
    receivedString = receivedString + c; 
  }
  
}

void check_for_nano(){
      if (receivedString.length() > 0) {
    Serial1.println(receivedString);
    Serial.println(receivedString);
    receivedString = receivedString.substring(1); 

         int hyphenIndex = receivedString.indexOf('-');
        String pm10String = receivedString.substring(0, hyphenIndex);
        pm10 = pm10String.toFloat();

        String pm25String = receivedString.substring(hyphenIndex + 1);
        pm25 = pm25String.toFloat();
    Serial.println(pm10);
    Serial.println(pm25);
    receivedString = "";
  }
}



void setColor(int redValue, int greenValue, int blueValue) {
  analogWrite(redPin, redValue);
  analogWrite(greenPin, greenValue);
  analogWrite(bluePin, blueValue);
}

void check_RGB(){
  if (digitalRead(BTpin)==HIGH && alarma == 0){
  setColor(0, 255, 0); // Green Color
}
if(digitalRead(BTpin)==LOW && alarma == 0){
  setColor(0, 0 , 255); // Blue Color
}
}


void check_buttons(){
  if(digitalRead(button_pin_earthquake) == pressed && led_earthquake_flag == false && alarma == 0){
  digitalWrite(LED_Earthquake, HIGH);
  earthquake_flag = true;
  led_earthquake_flag = true;
  while(digitalRead(button_pin_earthquake) == pressed){
        if(digitalRead(button_pin_gas) == pressed)
    start_alarms();
  }
}

if(digitalRead(button_pin_earthquake) == pressed && led_earthquake_flag == true && alarma == 0){
  digitalWrite(LED_Earthquake, LOW);
  led_earthquake_flag = false;
  earthquake_flag = false;
  while(digitalRead(button_pin_earthquake) == pressed){
    if(digitalRead(button_pin_gas) == pressed)
    start_alarms();
  }
}


if(digitalRead(button_pin_gas) == pressed && led_gas_flag == false && alarma == 0){
  digitalWrite(LED_Gas, HIGH);
  led_gas_flag = true;
  gas_flag = true;
  while(digitalRead(button_pin_gas) == pressed){
    if(digitalRead(button_pin_earthquake) == pressed)
    start_alarms();
  }
}

if(digitalRead(button_pin_gas) == pressed && led_gas_flag == true && alarma == 0){
  digitalWrite(LED_Gas, LOW);
  led_gas_flag = false;
  gas_flag = false;
  while(digitalRead(button_pin_gas) == pressed){
    if(digitalRead(button_pin_earthquake) == pressed)
    start_alarms();
  }
}

if(digitalRead(button_pin_earthquake) == pressed && digitalRead(button_pin_gas) == pressed && alarma == 1){
stop_alarms();
}


}


void stop_alarms(){
      digitalWrite(controlPin, LOW);
      digitalWrite(Relaypin, LOW); 
      digitalWrite(Relaypin2, LOW); 
      myservo.write(45);
      alarma = 0;
      Gas_CH4 = 0;
      Gas_CO = 0;
      Gas_C2H5OH = 0;
      Gas_C3H8 = 0;
      Gas_C4H10 = 0;
      Gas_H2 = 0;
      Gas_H2S = 0;
      Gas_NH3 = 0;
      write_to_mega = false;
      earthquake = false;
      servo_start = 0;
      delay(3000);
}

void start_alarms(){
      alarma = 1;
    digitalWrite(LED_Gas, HIGH);
    digitalWrite(LED_Earthquake, HIGH);
    setColor(255, 0 , 0);
    digitalWrite(controlPin, HIGH);
    digitalWrite(Relaypin, HIGH);   
    digitalWrite(Relaypin2, HIGH);

    if (write_to_mega == false) {
      Serial1.println("@asdf");
      write_to_mega = true;
    }
    if (servo_start == 0) {
      for (pos = 50; pos <= 180; pos += 1) {
        myservo.write(pos);
        delay(50);
        if (pos == 180) {
          servo_start = 1;
          break;
        }
      }
    }
}


int calculateAverage() {
  float totalSum = 0;
  int count = 20;
  for (int i = 0; i < count; i++) {
    lis.read();
    sensors_event_t event;
    lis.getEvent(&event);

    // Calculam acceleratia folosindune de axele corespunzatoare pozitiei, pentru cazurile in care senzorul este lipi de perete vom avea nevoie de axele x si z
    x = event.acceleration.x;
    z = event.acceleration.z;
    x = x + 2;
    z = z + 2;

    tot = x * y;
    tot = tot * 1000;
    tot = int(tot);

    totalSum += tot;

    delay(200);  
  }

  avg =  int(totalSum / count);
  return avg;
}

void checkNext20Values(int upperLimit, int lowerLimit) {
  int countBeyondLimits = 0;
  int totalChecks = 20;

  for (int i = 0; i < totalChecks; i++) {
    lis.read();
    sensors_event_t event;
    lis.getEvent(&event);

    
    x = event.acceleration.x;
    z = event.acceleration.z;
    x = x + 2;
    z = z + 2;

    tot = x * y;
    tot = tot * 1000;
    tot = int(tot);

    if (tot > upperLimit || tot < lowerLimit) {
      countBeyondLimits++;
    }

    delay(200);  
  }

  if (countBeyondLimits >= 15) {
    earthquake = true;
    start_alarms();
    setColor(255, 0 , 0);
  }
}

void check_earthquake(){
  
  lis.read();
  sensors_event_t event;
  lis.getEvent(&event);


 
  x = event.acceleration.x;
  z = event.acceleration.z;
  x = x + 2;
  z = z + 2;

  tot = x *z;
  tot = tot * 1000;
  tot = int(tot);
  if (tot > avg + 500 || tot < avg - 500) {
    checkNext20Values(avg + 500, avg - 500);
  }
  delay(200);
}

void verifySensors() {
  String sensorStatus = "#";
  // Verify lis sensor
  if (!lis.begin(0x18)) {
    Serial.println("Lis sensor not found :(");
    sensorStatus += "0";
  } else {
    sensorStatus += "1";
  }


  // Verify SGP30 sensor
  if (!sgp.begin(&Wire1)) {
    Serial.println("SGP30 sensor not found :(");
    sensorStatus += "0";
  } else {
    sensorStatus += "1";
  }

  // Verify MICS sensor
  if (!mics.begin()) {
    Serial.println("MICS sensor not found :(");
    sensorStatus += "0";
  } else {
    sensorStatus += "1";
  }

  // Verify BME680 sensor
  if (!bme.begin()) {
    Serial.println("BME680 sensor not found :(");
    sensorStatus += "0";
  } else {
    sensorStatus += "1";
  }

if(!sensorStatus.equals("#1111")){
   setColor(255, 0 , 0);
}
  Serial1.println(sensorStatus);

}

void gas(){
  if((mics.getGasExist(CH4) == EXIST) ||
    (mics.getGasExist(CO) == EXIST) || 
    (mics.getGasExist(C2H5OH) == EXIST) ||
    (mics.getGasExist(C3H8) == EXIST) || 
    (mics.getGasExist(C3H8) == EXIST) || 
    (mics.getGasExist(C4H10) == EXIST) || 
    (mics.getGasExist(H2) == EXIST) || 
    (mics.getGasExist(H2S) == EXIST) ||
    (mics.getGasExist(NH3) == EXIST)){
      Gas_CH4 = 0;
      Gas_CO = 0;
      Gas_C2H5OH = 0;
      Gas_C3H8 = 0;
      Gas_C4H10 = 0;
      Gas_H2 = 0;
      Gas_H2S = 0;
      Gas_NH3 = 0;
    check_for_gas();
  }

    volt = analogRead(A1);
    voltage = map(volt,0,1023, 0, 2500) + offset;
    voltage /=100;
    if(voltage > 9 || digitalRead(A3) == 0){
      start_alarms();
    }
}

void check_for_gas() {
  int8_t gasFlagCH4 = mics.getGasExist(CH4);
  if (gasFlagCH4 == EXIST) {
    Gas_CH4++;
    
  }

  int8_t gasFlagCO = mics.getGasExist(CO);
  if (gasFlagCO == EXIST) {
    Gas_CO++;
  }


  int8_t gasFlagC2H5OH = mics.getGasExist(C2H5OH);
  if (gasFlagC2H5OH == EXIST) {
    Gas_C2H5OH++;
  }


  int8_t gasFlagC3H8 = mics.getGasExist(C3H8);
  if (gasFlagC3H8 == EXIST) {
    Gas_C3H8++;
  }



  int8_t gasFlagC4H10 = mics.getGasExist(C4H10);
  if (gasFlagC4H10 == EXIST) {
    Gas_C4H10++;
  }



  int8_t gasFlagH2 = mics.getGasExist(H2);
  if (gasFlagH2 == EXIST) {
    Gas_H2++;
  }



  int8_t gasFlagH2S = mics.getGasExist(H2S);
  if (gasFlagH2S == EXIST) {
    Gas_H2S++;
  }



  int8_t gasFlagNH3 = mics.getGasExist(NH3);
  if (gasFlagNH3 == EXIST) {
    Gas_NH3++;
  }

   if (Gas_CH4 > 4 || Gas_CO > 4 || Gas_C2H5OH > 4 || Gas_C3H8 > 4 || Gas_C4H10 > 4 || Gas_H2 > 4 || Gas_H2S > 4 || Gas_NH3 > 4 || voltage > 9) {

    start_alarms();
  }
}

void splitString(String combined) {
  int spaceIndex = combined.indexOf(' ');
  
  value1 = combined.substring(0, spaceIndex);
  value2 = combined.substring(spaceIndex + 1);
}

void check_for_serial() {
  if (Serial1.available()) {
    String receivedData = Serial1.readStringUntil('\n');
    char secondLetter = receivedData.charAt(1);
    char firstLetter = receivedData.charAt(0);
    if(firstLetter == '!'){
      WiFi.status();
      if(status == WL_CONNECTED) {
        IP = WiFi.localIP().toString();
        if(strcmp(IP.c_str(), "0.0.0.0") == 0){
          IP = "~q";
        Serial1.println(IP);
        }else{
        IP = "(" + WiFi.localIP().toString();
         Serial1.println(IP);
      }
      }else{
        IP = ")q";
        Serial1.println(IP);
      }
    }
    if (firstLetter == '%') {
      receivedData = receivedData.substring(1);
      splitString(receivedData);

      char ssid[value1.length() + 1];
      char pass[value2.length() + 1];

         for (int x = 0; x < sizeof(ssid); x++) { 
        ssid[x] = value1[x]; 
    } 
             for (int x = 0; x < sizeof(pass); x++) { 
        pass[x] = value2[x]; 
    } 


if (WiFi.status() == WL_NO_MODULE) {
         IP = "~q";
        Serial1.println(IP);
  }

    while (status != WL_CONNECTED) {
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(value1);
    status = WiFi.begin(ssid, pass);
        delay(10000);
  }

    
  IP = "$" + WiFi.localIP().toString();

  Serial1.println(IP);

 
  server.begin();
    }
    if(firstLetter == '^'){
     
    WiFi.disconnect();
    WiFi.end();
    Serial.println("ajuns");
    }
    if (firstLetter == 'w') {
      bucla = 1;
    }
    if (firstLetter == 's') {
     stop_alarms();
    }
        if (firstLetter == '*') {
      verifySensors();
    }

    while (bucla == 1) {
      receivedData = Serial1.readStringUntil('\n');
      secondLetter = receivedData.charAt(1);
      firstLetter = receivedData.charAt(0);
      if (firstLetter == 'x') {
        bucla = 0;
      }

      if (firstLetter == 'b' && secondLetter == '0') {
        digitalWrite(controlPin, HIGH);
      }
      if (firstLetter == 'b' && secondLetter == '1') {
        digitalWrite(controlPin, LOW);
      }
      if (firstLetter == 'p' && secondLetter == '0') {
        digitalWrite(Relaypin, HIGH);
      }
      if (firstLetter == 'p' && secondLetter == '1') {
        digitalWrite(Relaypin, LOW);
      }
      if (firstLetter == 'g' && secondLetter == '0') {
        digitalWrite(Relaypin2, HIGH);
      }
      if (firstLetter == 'g' && secondLetter == '1') {
        digitalWrite(Relaypin2, LOW);
      }
      if (firstLetter == 'a' && secondLetter == '0') {
        if (servo_start == 0) {
          for (pos = 50; pos <= 180; pos += 1) {
            myservo.write(pos);
            delay(50); 
              if (pos == 180) {
              servo_start = 1;
              break;
            }
          }
        }
      }
      if (firstLetter == 'a' && secondLetter == '1') {
        myservo.write(50);
        servo_start = 0;
      }
    }
  }
}



  void handleWebClient(WiFiClient client) {
  Serial.println("New client connected");
  String currentLine = "";

  while (client.connected()) {
    
    if (client.available()) {
     
      char c = client.read();
      Serial.write(c);

      if (c == '\n') {
        if (currentLine.length() == 0) {
         
          client.println("HTTP/1.1 200 OK");
          client.println("Content-type:text/html");
          client.println("Connection: close"); 
          client.println();
          client.println("<!DOCTYPE HTML>");
          client.println("<html>");
          client.println("<head>");
          client.println("<title>Arduino WiFi Server</title>");
          client.println("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
          client.println("<style>");
          client.println("body { font-family: Arial, sans-serif; text-align: center; }");
          client.println("h1 { color: #333; }");
          client.println("table { border-collapse: collapse; margin: 0 auto; width: 80%; height: 50vh;}");
          client.println("th, td { border: 1px solid #333; padding: 8px; }");
          client.println("th { background-color: #f2f2f2; }");
          client.println(".green { color: green; }"); 
          client.println(".red { color: red; }");   
          client.println(".high { background-color: lightcoral; }"); 
          client.println(".normal { background-color: lightgreen; }"); 
          client.println("input[type='submit'] { font-size: 8vw; padding: 1vw 2vw; width: 40%; height: 30%; }");  
          client.println("</style>");
          client.println("</head>");
          client.println("<body>");
          client.println("<h1>Sensor Values</h1>");
          client.println("<table>");
          client.println("<tr><th>Parametru</th><th>Valori</th></tr>");

          client.print("<tr><td>Temperatura</td><td class=\"");
          client.print((temp > 10 && temp < 30) ? "normal" : "high");
          client.println("\">" + String(temp) + " *C</td></tr>");

         client.println("<tr><td>Presiune</td><td class=\"normal\">" + String(pres) + " hPa</td></tr>");

          client.print("<tr><td>Umiditate</td><td class=\"");
          client.print((hum > 20 && hum < 60) ? "normal" : "high");
          client.println("\">" + String(hum) + " %</td></tr>"); 

          client.print("<tr><td>Tvoc</td><td class=\"");
          client.print(tvc > 400 ? "high" : "normal");
          client.println("\">" + String(tvc) + " ppb</td></tr>"); 

          client.print("<tr><td>Eco2</td><td class=\"");
          client.print(ec > 1000 ? "high" : "normal");
          client.println("\">" + String(ec) + " ppm</td></tr>"); 

          client.print("<tr><td>PM10</td><td class=\"");
          client.print(pm10 > 35 ? "high" : "normal"); 
          client.println("\">" + String(pm10) + "</td></tr>");


         client.print("<tr><td>PM2.5</td><td class=\"");
         client.print(pm25 > 35 ? "high" : "normal"); 
         client.println("\">" + String(pm25) + "</td></tr>");

          client.println("</table>");
          client.println("<form><input type=\"submit\" value=\"Refresh\"></form>");
          client.println("</body>");
          client.println("</html>");

          
          delay(1000);
          break;
        } else {
          currentLine = "";
        }
      } else if (c != '\r') {
        currentLine += c;
      }
    }
  }


  client.stop();
  Serial.println("Client disconnected");
}



void time_interval()
{
    unsigned long elapsedTime = millis() - startTime;  
  if (elapsedTime >= hoursToMillis) {

    verifySensors();

    startTime = millis();
  }
}
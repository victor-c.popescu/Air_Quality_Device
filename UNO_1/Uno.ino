#include <Wire.h>
#include <SPI.h>

#include "Functii.h"


#define SEALEVELPRESSURE_HPA (1013.25)
#define CALIBRATION_TIME 3

String Eco2, Con, TVOC, Temperatura, Presiune, Umiditate;


unsigned long duration;
unsigned long starttime;
unsigned long sampletime_ms = 5000;
unsigned long lowpulseoccupancy = 0;
float ratio = 0;
float concentration = 0;

int q = 0;

unsigned long timer = 0;

void setup() {


  Serial.begin(38400);
  Serial1.begin(38400);
   pinMode(8,INPUT);
    Wire.begin(9); 
    Wire.onReceive(receiveEvent);
  Wire1.begin();


   pinMode(redPin, OUTPUT);
   pinMode(greenPin, OUTPUT);
   pinMode(bluePin, OUTPUT);
   pinMode(BTpin, INPUT); 
   pinMode(button_pin_earthquake, INPUT_PULLUP);
   pinMode(button_pin_gas, INPUT_PULLUP);
   pinMode(LED_Earthquake, OUTPUT);
   pinMode(LED_Gas, OUTPUT);

   digitalWrite(LED_Earthquake, HIGH);
   digitalWrite(LED_Gas, HIGH);
   setColor(255, 255, 255);
   
  pinMode(Relaypin, OUTPUT);
  digitalWrite(Relaypin, LOW);

  pinMode(Relaypin2, OUTPUT);
  digitalWrite(Relaypin2, LOW);

  pinMode(controlPin, OUTPUT);
  digitalWrite(controlPin, LOW);
  
  myservo.attach(9);
  myservo.write(50);

  starttime = millis();

  //verifySensors();

    if (!lis.begin(0x18)) {
    Serial.println("Couldnt start");
    while (1) yield();
  }
  Serial.println("LIS3DH found!");

  lis.setRange(LIS3DH_RANGE_2_G);
  lis.setDataRate(LIS3DH_DATARATE_400_HZ);

  calculateAverage();
  
  if (!sgp.begin(&Wire1)) {
       Serial.println("Sensor not found :(");
    while (1);
    // while(!Serial);
  }
    while (!mics.begin()) {
        Serial.println("NO Deivces !");
      delay(1000);
    }
    Serial.println("Device connected successfully !");

  if (!bme.begin()) {
    Serial.println("Could not find a valid BME680 sensor, check wiring!");
    while (1);
  }
  
 
  bme.setTemperatureOversampling(BME680_OS_8X);
  bme.setHumidityOversampling(BME680_OS_2X);
  bme.setPressureOversampling(BME680_OS_4X);
  bme.setIIRFilterSize(BME680_FILTER_SIZE_3);
  bme.setGasHeater(320, 150);  // 320*C for 150 ms


  uint8_t mode = mics.getPowerState();

    if(mode == SLEEP_MODE){
    mics.wakeUpMode();
    Serial.println("wake up sensor success!");
  }else{
    Serial.println("The sensor is wake up mode");
  }
  
   while(!mics.warmUpTime(CALIBRATION_TIME)){
    delay(1000);
  }
  

  
}


void loop() {


check_for_nano();

check_RGB();

check_buttons();

if(earthquake_flag == true){
check_earthquake();
}

if(gas_flag == true){
 gas();
}

  check_for_serial();

  time_interval();

  sgp.IAQmeasure();
  sgp.IAQmeasureRaw();

   temp = bme.temperature;
   pres = bme.pressure / 100.0;
   hum = bme.humidity;

   ec = sgp.eCO2;
   tvc = sgp.TVOC;

   Eco2 = "E = " + String(ec);
   TVOC = "TVOC = " + String(tvc);
   Con = "C = " + String(concentration);
   Temperatura = "Temp = " + String(temp);
   Presiune = "P = " + String(pres);
   Umiditate = "U = " + String(hum);



  WiFiClient client = server.available();
  if (client && client.connected() && client.available()) {
    handleWebClient(client);
  }


  if (alarma == 0) {

    Serial1.println(Temperatura);
    delay(100);
    Serial1.println(Presiune);
    delay(100);
    Serial1.println(Umiditate);
    delay(100);
    Serial1.println(TVOC);
    delay(100);
    Serial1.println(Eco2);
  }
  
}
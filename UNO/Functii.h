#ifndef FUNCTII_H
#define FUNCTII_H


#include <Adafruit_Sensor.h>
#include <Adafruit_LIS3DH.h>
#include "Adafruit_SGP30.h"
#include "Adafruit_BME680.h"
#include "DFRobot_MICS.h"
#include <Servo.h>


extern Adafruit_BME680 bme;
extern Adafruit_SGP30 sgp;
extern DFRobot_MICS_ADC mics;
extern Adafruit_LIS3DH lis;
extern Servo myservo;

extern int alarma;
extern int Relaypin;
extern int Relaypin2;
extern const int controlPin;
extern unsigned long startTime;


int calculateAverage();

void checkNext20Values(int upperLimit, int lowerLimit);

void check_earthquake();

String verifySensors();

void check_for_gas();

void check_for_serial();

void time_interval();

#endif

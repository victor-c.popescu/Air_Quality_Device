#include "Functii.h"




Adafruit_BME680 bme(&Wire1);
Adafruit_SGP30 sgp;
Adafruit_LIS3DH lis = Adafruit_LIS3DH(&Wire1);
#define ADC_PIN A0
#define POWER_PIN 7
Servo myservo;

int Gas_CH4 = 0;
int Gas_CO = 0;
int Gas_C2H5OH = 0;
int Gas_C3H8 = 0;
int Gas_C4H10 = 0;
int Gas_H2 = 0;
int Gas_H2S = 0;
int Gas_NH3 = 0;
int alarma = 0;
int Relaypin = 10;
int Relaypin2 = 11;
int servo_start = 0;
int pos = 45;
int bucla = 0;
unsigned long startTime;
const unsigned long hoursToMillis = 6 * 60 * 60 * 1000; // 6 hours in milliseconds
//const unsigned long hoursToMillis = 180000;  // 3 min in milliseconds`
const int controlPin = 4;
bool write_to_mega = false;
bool earthquake = false;
int avg;
float x, y, tot;
int offset =20;
int volt;
double voltage;


DFRobot_MICS_ADC mics(/*adcPin*/ ADC_PIN, /*powerPin*/ POWER_PIN);



int calculateAverage() {
  float totalSum = 0;
  int count = 20;
  for (int i = 0; i < count; i++) {
    lis.read();
    sensors_event_t event;
    lis.getEvent(&event);

    // Calculate total acceleration
    x = event.acceleration.x;
    y = event.acceleration.y;
    x = x + 2;
    y = y + 2;

    tot = x * y;
    tot = tot * 1000;
    tot = int(tot);

    totalSum += tot;

    delay(200);  // Maintain the same delay as in loop
  }

  avg =  int(totalSum / count);
  return avg;
}

void checkNext20Values(int upperLimit, int lowerLimit) {
  int countBeyondLimits = 0;
  int totalChecks = 20;

  for (int i = 0; i < totalChecks; i++) {
    lis.read();
    sensors_event_t event;
    lis.getEvent(&event);

    // Calculate total acceleration
    x = event.acceleration.x;
    y = event.acceleration.y;
    x = x + 2;
    y = y + 2;

    tot = x * y;
    tot = tot * 1000;
    tot = int(tot);

    if (tot > upperLimit || tot < lowerLimit) {
      countBeyondLimits++;
    }

    delay(200);  // Maintain the same delay as in loop
  }

  if (countBeyondLimits >= 15) {
    earthquake = true;
  }
}

void check_earthquake(){
  
  lis.read();
  sensors_event_t event;
  lis.getEvent(&event);


  //Serial.println(avg);

  // Calculate total acceleration
  x = event.acceleration.x;
  y = event.acceleration.y;
  x = x + 2;
  y = y + 2;


  tot = x * y;
  tot = tot * 1000;
  tot = int(tot);
  if (tot > avg + 500 || tot < avg - 500) {
    checkNext20Values(avg + 500, avg - 500);
  }
  //Serial.println(tot,0);

  delay(200);

}

String verifySensors() {
  String sensorStatus = "#";
  // Verify SGP30 sensor
  if (!sgp.begin()) {
    Serial.println("SGP30 sensor not found :(");
    sensorStatus += "0";
  } else {
    sensorStatus += "1";
  }

  // Verify MICS sensor
  if (!mics.begin()) {
    Serial.println("MICS sensor not found :(");
    sensorStatus += "0";
  } else {
    sensorStatus += "1";
  }

  // Verify BME680 sensor
  if (!bme.begin()) {
    Serial.println("BME680 sensor not found :(");
    sensorStatus += "0";
  } else {
    sensorStatus += "1";
  }

  Serial1.println(sensorStatus);

  return sensorStatus;
}



void check_for_gas() {
  int8_t gasFlagCH4 = mics.getGasExist(CH4);
  if (gasFlagCH4 == EXIST) {
    Gas_CH4++;
    
  }

  int8_t gasFlagCO = mics.getGasExist(CO);
  if (gasFlagCO == EXIST) {
    Gas_CO++;
  }


  int8_t gasFlagC2H5OH = mics.getGasExist(C2H5OH);
  if (gasFlagC2H5OH == EXIST) {
    Gas_C2H5OH++;
  }


  int8_t gasFlagC3H8 = mics.getGasExist(C3H8);
  if (gasFlagC3H8 == EXIST) {
    Gas_C3H8++;
  }



  int8_t gasFlagC4H10 = mics.getGasExist(C4H10);
  if (gasFlagC4H10 == EXIST) {
    Gas_C4H10++;
  }



  int8_t gasFlagH2 = mics.getGasExist(H2);
  if (gasFlagH2 == EXIST) {
    Gas_H2++;
  }



  int8_t gasFlagH2S = mics.getGasExist(H2S);
  if (gasFlagH2S == EXIST) {
    Gas_H2S++;
  }



  int8_t gasFlagNH3 = mics.getGasExist(NH3);
  if (gasFlagNH3 == EXIST) {
    Gas_NH3++;
  }

    volt = analogRead(A1);
    voltage = map(volt,0,1023, 0, 2500) + offset;// map 0-1023 to 0-2500 and add correction offset
    voltage /=100;// divide by 100 to get the decimal values


  if (Gas_CH4 > 4 || Gas_CO > 4 || Gas_C2H5OH > 4 || Gas_C3H8 > 4 || Gas_C4H10 > 4 || Gas_H2 > 4 || Gas_H2S > 4 || Gas_NH3 > 4 || earthquake == true || voltage > 9) {

    alarma = 1;
    digitalWrite(controlPin, HIGH);
    digitalWrite(Relaypin, HIGH);   // Sends high signal
    digitalWrite(Relaypin2, HIGH);  // Sends high signal
    if (write_to_mega == false) {
      Serial1.println("@asdf");
      write_to_mega = true;
      Serial.print("DAAAAAAAA");
    }
    if (servo_start == 0) {
      for (pos = 50; pos <= 180; pos += 1) {
        myservo.write(pos);
        delay(50);  // Increase the delay to slow down the movement (adjust as needed)
        if (pos == 180) {
          servo_start = 1;
          break;
        }
      }
    }
  }
}



void check_for_serial() {
  if (Serial1.available()) {
    String receivedData = Serial1.readStringUntil('\n');
    char secondLetter = receivedData.charAt(1);
    char firstLetter = receivedData.charAt(0);
    if (firstLetter == 'w') {
      bucla = 1;
    }
    if (firstLetter == 's') {
      digitalWrite(controlPin, LOW);
      digitalWrite(Relaypin, LOW); // Makes the signal low
      digitalWrite(Relaypin2, LOW);  // Makes the signal low
      myservo.write(45);
      alarma = 0;
      Gas_CH4 = 0;
      Gas_CO = 0;
      Gas_C2H5OH = 0;
      Gas_C3H8 = 0;
      Gas_C4H10 = 0;
      Gas_H2 = 0;
      Gas_H2S = 0;
      Gas_NH3 = 0;
      write_to_mega = false;
      earthquake = false;
      servo_start = 0;
    }
        if (firstLetter == '*') {
      verifySensors();
    }

    while (bucla == 1) {
      receivedData = Serial1.readStringUntil('\n');
      secondLetter = receivedData.charAt(1);
      firstLetter = receivedData.charAt(0);
      if (firstLetter == 'x') {
        bucla = 0;
      }

      if (firstLetter == 'b' && secondLetter == '0') {
        digitalWrite(controlPin, HIGH);
      }
      if (firstLetter == 'b' && secondLetter == '1') {
        digitalWrite(controlPin, LOW);
      }
      if (firstLetter == 'p' && secondLetter == '0') {
        digitalWrite(Relaypin, HIGH);
      }
      if (firstLetter == 'p' && secondLetter == '1') {
        digitalWrite(Relaypin, LOW);
      }
      if (firstLetter == 'g' && secondLetter == '0') {
        digitalWrite(Relaypin2, HIGH);
      }
      if (firstLetter == 'g' && secondLetter == '1') {
        digitalWrite(Relaypin2, LOW);
      }
      if (firstLetter == 'a' && secondLetter == '0') {
        if (servo_start == 0) {
          for (pos = 50; pos <= 180; pos += 1) {
            myservo.write(pos);
            delay(50);  // Increase the delay to slow down the movement (adjust as needed)
              if (pos == 180) {
              servo_start = 1;
              break;
            }
          }
        }
      }
      if (firstLetter == 'a' && secondLetter == '1') {
        myservo.write(50);
        servo_start = 0;
      }
    }
  }
}



void time_interval()
{
    unsigned long elapsedTime = millis() - startTime;  // Calculate elapsed time
  if (elapsedTime >= hoursToMillis) {

    verifySensors();
    // Reset the timer
    startTime = millis();
  }
}
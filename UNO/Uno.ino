#include <Wire.h>
#include <SPI.h>
#include <WiFiS3.h>
#include "Functii.h"


#define BME_SCK 13
#define BME_MISO 12
#define BME_MOSI 11
#define BME_CS 10

#define SEALEVELPRESSURE_HPA (1013.25)
#define CALIBRATION_TIME 3

char ssid[] = "DIGI-468u";
char pass[] = "MNbPcB5m";

String Eco2, Con, TVOC, Temperatura, Presiune, Umiditate;
int ec, tvc, hum, pres, temp, index;
String receivedString = "";
int pin = 8;
unsigned long duration;
unsigned long starttime;
unsigned long sampletime_ms = 5000;
unsigned long lowpulseoccupancy = 0;
float ratio = 0;
float concentration = 0;

int status = WL_IDLE_STATUS;
WiFiServer server(80);
int q = 0;

void receiveEvent(int howMany) {
  
  
  
   while (Wire.available()) {
    char c = Wire.read(); // Read each byte received over I2C
    receivedString += c; // Append the byte to the receivedString variable
  }
  
}
unsigned long timer = 0;

void setup() {


  Serial.begin(38400);
  Serial1.begin(38400);
   pinMode(8,INPUT);
    Wire.begin(9); 
    Wire.onReceive(receiveEvent);
  // Attach a function to trigger when something is received.
  Wire1.begin();

    if (WiFi.status() == WL_NO_MODULE) {
    Serial.println("Communication with WiFi module failed!");
    while (true);
  }

    while (status != WL_CONNECTED) {
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(ssid);
    status = WiFi.begin(ssid, pass);
        delay(10000);
  }

    // You're connected to the network
  Serial.print("Connected to WiFi. IP address: ");
  Serial.println(WiFi.localIP());

  // Start the server
  server.begin();

  pinMode(Relaypin, OUTPUT);
  digitalWrite(Relaypin, LOW);

  pinMode(Relaypin2, OUTPUT);
  digitalWrite(Relaypin2, LOW);

  pinMode(controlPin, OUTPUT);
  digitalWrite(controlPin, LOW);
  
  myservo.attach(9);
  myservo.write(50);

  starttime = millis();//get the current time; 

  //verifySensors();

    if (!lis.begin(0x18)) {  // change this to 0x19 for alternative i2c address
    Serial.println("Couldnt start");
    while (1) yield();
  }
  Serial.println("LIS3DH found!");

  lis.setRange(LIS3DH_RANGE_2_G);
  lis.setDataRate(LIS3DH_DATARATE_400_HZ);

  calculateAverage();
  
  if (!sgp.begin(&Wire1)) {
       Serial.println("Sensor not found :(");
    while (1);
    // while(!Serial);
  }
    while (!mics.begin()) {
        Serial.println("NO Deivces !");
      delay(1000);
    }
    Serial.println("Device connected successfully !");

  if (!bme.begin()) {
    Serial.println("Could not find a valid BME680 sensor, check wiring!");
    while (1);
  }
  
 
  bme.setTemperatureOversampling(BME680_OS_8X);
  bme.setHumidityOversampling(BME680_OS_2X);
  bme.setPressureOversampling(BME680_OS_4X);
  bme.setIIRFilterSize(BME680_FILTER_SIZE_3);
  bme.setGasHeater(320, 150);  // 320*C for 150 ms


  uint8_t mode = mics.getPowerState();

    if(mode == SLEEP_MODE){
    mics.wakeUpMode();
    Serial.println("wake up sensor success!");
  }else{
    Serial.println("The sensor is wake up mode");
  }
  /*
   while(!mics.warmUpTime(CALIBRATION_TIME)){
    delay(1000);
  }
  */

  
}

void handleWebClient(WiFiClient client) {
  Serial.println("New client connected");
  String currentLine = "";

  while (client.connected()) {
    
    if (client.available()) {
     
      char c = client.read();
      Serial.write(c);

      if (c == '\n') {
        if (currentLine.length() == 0) {
          // Send HTTP response
          client.println("HTTP/1.1 200 OK");
          client.println("Content-type:text/html");
          client.println("Connection: close"); // Close connection after response
          client.println();
          client.println("<!DOCTYPE HTML>");
          client.println("<html>");
          client.println("<head>");
          client.println("<title>Arduino WiFi Server</title>");
          client.println("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
          client.println("<style>");
          client.println("body { font-family: Arial, sans-serif; text-align: center; }");
          client.println("h1 { color: #333; }");
          client.println("table { border-collapse: collapse; margin: 0 auto; width: 80%; height: 50vh;}");
          client.println("th, td { border: 1px solid #333; padding: 8px; }");
          client.println("th { background-color: #f2f2f2; }");
          client.println(".green { color: green; }"); // Define green color class
          client.println(".red { color: red; }");     // Define red color class
          client.println(".high { background-color: lightcoral; }"); // Define background color class for high temperature
          client.println(".normal { background-color: lightgreen; }"); // Define background color class for normal temperature
          client.println("input[type='submit'] { font-size: 8vw; padding: 1vw 2vw; width: 40%; height: 30%; }"); // Set size of the button as a percentage 
          client.println("</style>");
          client.println("</head>");
          client.println("<body>");
          client.println("<h1>Sensor Values</h1>");
          client.println("<table>");
          client.println("<tr><th>Parametru</th><th>Valori</th></tr>");

          client.print("<tr><td>Voc</td><td class=\"");
          client.print(index > 150 ? "high" : "normal");
          client.println("\">" + String(index) + "</td></tr>"); // Add class based on temperature value

          client.print("<tr><td>Temperatura</td><td class=\"");
          client.print((temp > 10 && temp < 30) ? "normal" : "high");
          client.println("\">" + String(temp) + " *C</td></tr>"); // Add class based on temperature value

         client.println("<tr><td>Presiune</td><td class=\"normal\">" + String(pres) + " hPa</td></tr>");

          client.print("<tr><td>Umiditate</td><td class=\"");
          client.print((hum > 20 && hum < 60) ? "normal" : "high");
          client.println("\">" + String(hum) + " %</td></tr>"); // Add class based on temperature value

          client.print("<tr><td>Tvoc</td><td class=\"");
          client.print(tvc > 400 ? "high" : "normal");
          client.println("\">" + String(tvc) + " ppb</td></tr>"); // Add class based on temperature value

          client.print("<tr><td>Eco2</td><td class=\"");
          client.print(ec > 1000 ? "high" : "normal");
          client.println("\">" + String(ec) + " ppm</td></tr>"); // Add class based on temperature value

          client.println("</table>");
          client.println("<form><input type=\"submit\" value=\"Refresh\"></form>");
          client.println("</body>");
          client.println("</html>");

          // Break out of the while loop
          delay(1000);
          break;
        } else {
          currentLine = "";
        }
      } else if (c != '\r') {
        currentLine += c;
      }
    }
  }

  // Close the connection
  client.stop();
  Serial.println("Client disconnected");
}


void loop() {
    if (receivedString.length() > 0) {
    Serial1.println(receivedString);
    Serial.println(receivedString);
    
    // Optionally, you can reset the receivedString variable for the next reception
    receivedString = "";
  }

/*
duration = pulseIn(pin, LOW);
  lowpulseoccupancy = lowpulseoccupancy+duration;
 
  if ((millis()-starttime) > sampletime_ms)
  {
    ratio = lowpulseoccupancy/(sampletime_ms*10.0);  // Integer percentage 0=>100
    concentration = 1.1*pow(ratio,3)-3.8*pow(ratio,2)+520*ratio+0.62; // using spec sheet curve
    lowpulseoccupancy = 0;

  Serial.println("C:"+String(concentration)); 
 
 
 
 if (concentration < 1000) {
 

  Serial.println("Clean"); 
  }
 
  if (concentration > 1000 && concentration < 10000) {
 

  Serial.println("Good"); 
    }
 
     if (concentration > 10000 && concentration < 20000) {
 

  Serial.println("Acceptable"); 
    }
 
    if (concentration > 20000 && concentration < 50000) {

  Serial.println("Heavy"); 
  }
 
 if (concentration > 50000 ) {
 

  Serial.println("Hazard"); 
    } 
 
    starttime = millis();
  }
 
  
*/





check_earthquake();



  time_interval();

  sgp.IAQmeasure();
  sgp.IAQmeasureRaw();

   temp = bme.temperature;
   pres = bme.pressure / 100.0;
   hum = bme.humidity;

  float alt = bme.readAltitude(SEALEVELPRESSURE_HPA);
   ec = sgp.eCO2;
  int rw = sgp.rawH2;
  int re = sgp.rawEthanol;
   tvc = sgp.TVOC;

   Eco2 = "E = " + String(ec);
   TVOC = "TVOC = " + String(tvc);
   Con = "C = " + String(concentration);
   Temperatura = "Temp = " + String(temp);
   Presiune = "P = " + String(pres);
   Umiditate = "U = " + String(hum);

  check_for_gas();

  check_for_serial();



  WiFiClient client = server.available();
  if (client && client.connected() && client.available()) {
    handleWebClient(client);
  }




  if (alarma == 0) {
   // Serial1.println(Con);
    delay(100);
    Serial1.println(Temperatura);
    delay(100);
    Serial1.println(Presiune);
    delay(100);
    Serial1.println(Umiditate);
    delay(100);
    Serial1.println(TVOC);
    delay(100);
    Serial1.println(Eco2);
  }
  
}